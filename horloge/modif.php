<?php
session_start();
require "lg_" . $_SESSION["lg"] . ".php";
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
require "lg_" . $_SESSION["lg"] . ".php";
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
$lig = filter_input(INPUT_GET, "lig", FILTER_SANITIZE_STRING);
$filtre = filter_input(INPUT_GET, "fil", FILTER_SANITIZE_STRING);
$matin = '';
$soir = '';
if ($filtre == 2 ) {
    $matin = ' disabled';
} elseif ($filtre == 3 ) {
    $soir = ' disabled';
}
?>
<script type="text/javascript" src="/js/journee.js"></script>
<!-- <div style='margin-top:50px' id="but"> -->
<div style="width:400px;float:right" id='but'>
    <button id='heure'>Prochaine sonnerie à <span id='mdheure'><?php echo $req ?></span></button><br>
    <button id='heuson'>Modifier heure/sonnerie</button><br>
    <button id='sup'<?php echo $matin.$soir?>>Supprimer <?php echo $req ?></button><br>
    <button id='ajout'<?php echo $soir?>>Ajouter après <?php echo $req ?></button>
</div>
<button class='fermer' onclick="bascule()"><img src="sortie.png"></button>
<script>
    $(document).ready(function() {
        $('#but button').click(function() {
            switch ($(this).attr('id')) {
                case 'heure':
                    $('#info').html($('#mdheure').html());
                    bascule();
                    break;
                case 'heuson':
                    $('#vide').attr('alt', 'heuson');
                    charge('heuson', <?php echo $lig ?>, 'envoi');
                    break;
                case 'sup':
                    $('#vide').attr('alt', 'sup');
                    charge('heuson', <?php echo $lig ?>, 'envoi');
                    break;
                case 'ajout':
                    $('#vide').attr('alt', 'ajout');
                    charge('heuson', <?php echo $lig ?>, 'envoi');
                    break;
            }
        });
    });

    $('#but button').css('height', 370 / 4);
    // if ($('#mdheure').html() < $('#info').html()) {
    //     $('#heure').prop("disabled", true);
    //     $('#heuson').prop("disabled", true);
    // }
</script>