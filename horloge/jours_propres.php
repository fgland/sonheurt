<?php
session_start();
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
require "lg_".$_SESSION["lg"].".php"; 
//horaires propres pour certains jours, le code étant mois+jour+rechargement immédiat
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
if ($req !="") {
    ?>
    <script>
    charge('chargement', '<?php echo $req?>', 'envoi');
    </script>
    <?php
    exit;
}
?>
<script>
$(document).ready(function(){
    $('#but button').click(function() {
        $(this).prop("disabled",true);
        $('#affichage').css('display','none');
        charge('jours_propres',$(this).attr('code'),'envoi');
    });
});
</script>
<div style ="width:400px;float:right" id='but'>
<button code = '0325_0'><?php echo $lg[38]?></button><br>
<button class='buttonp' style="float:left" code = '97_0'><?php echo $lg[39]?></button>
<button class='buttonp' style="float:right" code = '99_0'><?php echo $lg[40]?></button>
<button class='buttonp' code = '98_0'><?php echo $lg[40]?></button>
<br>
<button class='buttonp' style="float:left" code = '101_0'><?php echo $lg[43]?></button>
<button class='buttonp' style="float:left" code = '100_0'><?php echo $lg[42]?></button>
<button class='buttonp' style="float:right" code = '1102_0'><?php echo $lg[44]?></button><br>
<button class='buttonp' style="float:left" code = '1223_0'><?php echo $lg[60]?></button>
<button class='buttonp' style="float:right" code = '1225_0'><?php echo $lg[46]?></button>
<button class='buttonp' code = '1224_0'><?php echo $lg[45]?></button>
<br>
<button class='buttondemi' style="float:left" code = '1226_0'><?php echo $lg[47]?></button>
<button class='buttondemi' style="float:right" code = '1227_0'><?php echo $lg[48]?></button><br>
<button class='buttonp' style="float:left" code = '1230_0'><?php echo $lg[61]?></button>
<button class='buttonp' style="float:left" code = '1231_0'><?php echo $lg[62]?></button>
<button class='buttonp' style="float:right" code = '0101_0'><?php echo $lg[63]?></button><br>

</div>
<button class='fermer' onclick="bascule()"><img src="sortie.png"></button>

<script>
$('#but button').css('height',370/6);
</script>