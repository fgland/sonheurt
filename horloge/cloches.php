<?php
session_start();
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
require "lg_" . $_SESSION["lg"] . ".php";
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
?>
<script>
    $(document).ready(function() {
        $('#but button').click(function() {
            $('#o').html($(this).html());
            $('#o').attr('code', $(this).attr('code'));
            $('#but').empty();
            $('#but').html(validation);
        });
    });
</script>
<?php
if ($req == 'ang') {
?>
    <div style="width:400px;" id='but'>
        <button class='buttondemi' style="float:left" code="Iškilmė 1 eilė_770_194"><?php echo $lg[8] ?></button>
        <button class='buttondemi' style="float:right" code="Šventė 2 eilė_310_125"><?php echo $lg[9] ?></button><br>
        <button code="V.Ang. 3 eilė_620_135"><?php echo $lg[10] ?></button><br>
        <button code="V. Ang. Sekmadienis_510_135"><?php echo $lg[11] ?></button><br>
        <button code="V. Ang. MARIJA_110_135"><?php echo $lg[12] ?></button><br>
        <button code="V.Ang.minėjimas_220_135"><?php echo $lg[13] ?></button><br>
        <button code="V. Ang. šiokmadienis_440_144"><?php echo $lg[14] ?></button><br>
    </div>
    <script>
        $('#but button').css('height', 250 / 6);
    </script>
<?php
} elseif ($req == 'tin') {
?>
    <div id='but'>
        <div style="width:450px;float:right">
            <h3>Tintements</h3>
            <button code="sk2+Varpas1/2/3_0xv_85"><?php echo $lg[24] ?></button><br>
            <button class="buttonp" code="Marija 1x12_01p_65"><?php echo $lg[25] ?></button><button class="buttonp" code="Marija 3x3_01m_35"><?php echo $lg[26] ?></button><button class="buttonp" code="sk2+Marija_0ut_85">sk2 + Marija</button><br>
            <button class="buttonp" code="Petras 1x12_02p_65"><?php echo $lg[27] ?></button><button class="buttonp" code="Petras 3x3_02m_35"><?php echo $lg[28] ?></button><button class="buttonp" code="sk2+Petras_0uu_85">sk2 + Petras</button><br>
            <button class="buttonp" code="Kazimieras 1x12_04p_65"><?php echo $lg[29] ?></button><button class="buttonp" code="Kazimieras 3x3_04m_35"><?php echo $lg[30] ?></button><button class="buttonp" code="sk2+Kazimieras_0uw_85">sk2 + Kazimieras</button><br>
            <button class="buttonp" style="float:left" code="Skambutis 1x10_03n_10"><?php echo $lg[31] ?></button>
            <button class="buttonp" style="float:left" code="Décès et volée funèbre_0xt_840"><?php echo $lg[64] ?></button>
            <button class="buttonp" style="float:right" code="Skambutis 3x5_03s_21"><?php echo $lg[32] ?></button><br>
        </div>
        <script>
            $('#but button').css('height', 250 / 6);
        </script>
    <?php
} elseif ($req == 'vol') {
    ?>
        <div id='but' style="width:420px;float:right">
            <button code="Volée Solennité_700_146"><?php echo $lg[15] ?></button><br>
            <button code="Volée Fêtes II_300_156"><?php echo $lg[16] ?></button><br>
            <button code="Volée Fêtes III_600_156"><?php echo $lg[17] ?></button><br>
            <button code="Volée Dimanche_500_156"><?php echo $lg[18] ?></button><br>
            <div style="width:420px;float:right">
                <button class="buttondemi" style="float:right" code="Varpas Marija_0vt_120"><?php echo $lg[19] ?></button>
                <button class="buttondemi" style="float:left" code="Varpas Marija_0wt_60"><?php echo $lg[20] ?></button>
                <button class="buttonp" code="Volée sib_400_90"><?php echo $lg[21] ?></button>
                <button class="buttonp" code="Volée sol_200_72"><?php echo $lg[22] ?></button>
                <button class="buttonp" code="Volée mib_100_60"><?php echo $lg[23] ?></button>
            </div>
        </div>
        <script>
            $('#but button').css('height', 250 / 6);
        </script>
    <?php
} elseif ($req == 'div') {
    ?>
        <div id='but' style="width:420px;float:right">
            <button code="Kazimieras+Sk_0tu_8_0">Lever</button><br>
            <button code="changement_00m_21_0">Changement de jour(matin)</button><br>
            <button code="changement_00o_1_0">Changement de jour(soir)</button><br>
            <button code="salut_00n_21_0">Salut</button><br>
            <button code="Fin jour_000_0_0">fin journée</button><br>
        </div>
        <script>
            $('#but button').css('height', 250 / 5);
        </script>
    <?php
}
    ?>