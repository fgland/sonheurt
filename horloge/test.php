<?php
session_start();
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$mois=array("","sausis","vasaris","kovas","balandis","gegužė","birželis","liepas","rugpjūtis","rugsėlis","spalis","lapkritis","gruodis");
$mois_ab=array("","sau.","vas.","kov.","bal.","geg.","birž.","liep.","rugp.","rugs.","spal.","lapk.","gruo.");
$sem=array("Sk","Pr","An","Tr","Kt","Pn","Št");

require "degres_".$_SESSION['lg'].".php";
$date = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);

$bisextile = 0;
if (!$date) {
    $date_ref=date('U');
} else {
    $date_ref=$date;
}

$nb_jour = 366;

require "annee.php";
if (date('L', $date_ref) == 0) {
    unset($annee[60]);//suppression du 29 février
    $nb_jour = 365;
}

$feteND = '';//1 désactive la possibilité de 'De sabbato'
//calcul du numero liturgique de la semaine
//les variable sont basé sur le n° du jour(z) dans l'année pour éviter les problèmes d'heure
//  ex : easterdate = 21/4/2019 12:00:00
$Zref = date('z', $date_ref);
$Zpaques = date('z', easter_date(date('Y', $date_ref)));
$Zcendres = ($Zpaques - 46);
$temps = 0;//temps ordinaire,1=carême,2=octave de Pâques,3=temps pascal
$avent = 0;//a revoir, il manque un $temps 
$time25dec = mktime(0, 0, 0, 12, 25, date('Y', $date_ref));
$Zdec25 = date('z', $time25dec);
$Wdec25 = date('w', $time25dec);
if ($Wdec25 == 0 ) {
    $Zavent1 = ($Zdec25 - $Wdec25 - 28);
} else {
    $Zavent1 = ($Zdec25 - $Wdec25 - 21);
}
$Z34 = $Zavent1 -7;
$debut_semaine = ($Zref- date('w', $date_ref));
$paques = easter_date(date('Y', $date_ref));
$mardi = "";
$num_semaine = '';

$i = 0;
// echo "<br>".$Zref." >  ".($Zcendres +4)." ".($Zpaques - 42);
//----------------------temps de Noël et Épyphanie---------------------------
if (((date('j', $date_ref) - date('w', $date_ref)) < 6)&&(date('n', $date_ref)==1)) {
    $temps = 0;
    $n_dimanche = 'Kalėdų laikas';
    $feteND = 1;
} elseif (((date('j', $date_ref) - date('w', $date_ref)) >= 6)&&
    (date('n', $date_ref)==1)&&(date('j', $date_ref) - date('w', $date_ref)) <= 13) {
        $temps = 0;
        $n_dimanche = "1 eilinė savaitė";    
//----------------------temps ordinaire avant les Cendres---------------------------
} elseif ($Zref <  ($Zpaques - 42)) { //on laisse la semaine jusqu'au dimanche
    //le premier dimanche suit le 6 janvier. Si le 6 est un dimanche, le premier sera le 13
    $w_janvier6 = date('w', mktime(0, 0, 0, 1, 6, date('Y')));
    $dimanche1_ord = 6+(7 - $w_janvier6);
    $debutz_ordinaire = (date('z', mktime(0, 0, 0, 1, $dimanche1_ord, date('Y'))));
    $n_dimanche = (int)((($Zref - $debutz_ordinaire) / 7)+1)." eilinė savaitė";
//--------------------------carême----------------------------------------------
} elseif (($Zref >= ($Zcendres +4))&&($Zref < ($Zpaques -7))) {
    $temps = 1;//mémoires affichées mais programme ferie
    $dimanche1_car = $Zpaques - 42;
    $n_d = (int)((($Zref - $dimanche1_car) / 7)+1);
    $n_dimanche = ($n_d == 6)?"Didžioji savaitė":$n_d." gavėnios savaitė";
//-----------------------semaine sainte-------------------------------------------
} elseif (($Zref >= ($Zpaques -7))&&($Zref < $Zpaques)) {
    $temps = 1;//mémoires affichées mais programme ferie
    $dimanche1_car = $Zcendres + 4;
    $n_d = (int)((($Zref - $dimanche1_car) / 7)+1);
    $n_dimanche = ($n_d == 6)?"Didžioji savaitė":$n_d." gavėnios savaitė";
//-----------------------octave de Pâques-------------------------------------------
} elseif ($debut_semaine == date('z', $paques)) {
    $temps = 2;
    $n_dimanche ="Velykų oktava";
    
//-----------------------temps pascal-------------------------------------------
} elseif ($Zref < ($Zpaques+49)) {
    $n_d = (int)((($Zref - $Zpaques) / 7)+1);
    $n_dimanche = $n_d." Velykų savaitė";
    $temps = 3;
//------------------temps ordinaire après Pâques--------------------------------
} elseif (($Zref > ($Zpaques+48))&&($Zref< $Zavent1)) {
    //cacul du n° à partir du 1er dimanche de l'Avent
    $n_d = (int)(34-(($Z34 - $Zref) / 7));
    $temps = 0;
    $n_dimanche =$n_d.' eilinė savaitė';
//------------------temps de l'avent--------------------------------
} else {
    $n_d = (int)(($Zref - $Zavent1)/7)+1;
    if ($n_d == 5 ) {
        $n_dimanche ='Kalėdų laikas';
    } else {
        $temps = 0;
        $avent = 1;
        $n_dimanche =$n_d.' adventas savaitė';
    }
}
//------------------temps de l'avent--------------------------------
// echo "<br>".(date('z', date($date_ref))-date('w', date($date_ref)))." ".date('j/n/Y', date($date_ref))." ".date('w', date($date_ref));//." < ".(date('z', $paques)+50)."--".(int)(((date('z', $date_ref) - date('z', $paques)) / 7));
?>
<br><br><br>
<center><table id="calendrier">
    <tr><th colspan="3" style='background-color:Lavender;text-align:center'>
    <button style='float:left'> < </button>
    <?php echo $n_dimanche." <button style='width:25px'><</button>".date('Y', date($date_ref))?><button style='width:25px'>></button>
    <button style='float:right'> > </button>
    </th></tr>
<?php
$salut = '';
// $n_d = '';
// echo $rq_saint->$champ." ".(date('z', $paques)+50);
// echo $debut_semaine;
$n = 0;
$s = 0;
if ($debut_semaine < 0) {
    $debut_semaine = $nb_jour + $debut_semaine;
}
$fin_semaine = $debut_semaine+8;
foreach ($annee as $value) {
        if(( $n > $debut_semaine)&&( $n < ($debut_semaine+8))) {
            //attention, le tableau php commence à [0], l'année commence à 1
            // echo $n." ".$value."<br>";
            $tab_value = explode("_", $value);
            $degre = $tab_value[3];//    |ces valeurs
            $saint = $tab_value[0];// |seront écrasées
            $deg = $degr[$tab_value[3]];//     |par les cas particuliers
                        // echo $n."<br>";
            $ext = '';//variable nom promenade/salut
            $ext_deg = '_0';//variable promenade(1)/salut(2)/jour emportant sur le dimanche(3)
            // echo $rq_saint->$champ." ".(date('z', $paques)+49)."<br>";
            if (($tab_value[4] == 1)||($tab_value[4] == 3)) {//=ND,3= ND solennite
                $feteND = 1;
            }
            switch ($s) {                
                //-----------------------Dimanche-----------------------------------
                case 0: if ($tab_value[4] > 1) {//fête l'emportant sur le dimanche
                    $saint = $tab_value[0];
                    // $deg = '';
                } elseif ((date('j', $date_ref)== 6)&&(date('n', $date_ref) == 1)) {//Baptème
                    $saint = 'Kristaus apsireiškimas';
                    $degre = 8;
                    $deg = $degr[$degre];
                } elseif (((date('j', $date_ref)- date('w', $date_ref)) <= 13)&&((date('j', $date_ref)- date('w', $date_ref)) > 6)&&($tab_value[2] == 1)) {//Baptème
                    $saint = 'Kristaus krikštas';
                    $degre = 8;
                    $deg = $degr[$degre];
                } elseif ($n == $Zpaques+1) {//Pâques
                    $saint = 'VELYKOS';
                    $degre = 100;
                    $deg = $degr[10];
                } elseif ($n == $Zpaques+8) {//Pâques
                    $saint = 'Atvelykis';
                    $degre = 9;
                    $deg = $degr[$degre];
                } elseif ($n == ($Zpaques+50)) {//Pentecôte
                    $saint = 'Šv. Dvasios atsiuntimas-Sekminės';
                    $degre = 10;
                    $deg = $degr[$degre];
                } elseif ($n == ($Zpaques+57)) {//Sainte Trinité
                    $saint = 'Švč. Trejybė ';
                    $degre = 9;
                    $deg = $degr[$degre];
                } elseif ($n_d == 34) {
                    //Christ-roi
                    $saint = 'Kristus karalius';
                    $degre = 9;
                    $deg = $degr[$degre];
                } elseif ((($tab_value[1] > 25) &&(($tab_value[1] < 31))&&($tab_value[2] == 12))) {//Sainte Famille
                        $saint = 'Šventoji Šeima';
                        $degre = 9;
                        $deg = $degr[9];/* echo 2; */
                } else {
                    $saint = '';
                    $degre = 8;
                    $deg = $degr[$degre];
                    $feteND = 0;
                }
                    break;
                //-------------------lundi----------------
                case 1: if ($n == $Zpaques + 2) {
                        $saint = 'Velykų II diena';
                        $degre = '101';
                        $deg = $degr[9];
                } elseif ($n == $Zpaques+51) {
                    $saint = "Švč. Mergelė Marija, Bažnyčios Motina";
                    $degre = '3';
                    $deg = $degr[3];
                    $feteND = 1;
                } elseif ($n == $Zpaques - 5) {//lundi saint
                    $saint = '';
                } elseif (($tab_value[2] == 6)&&($tab_value[1] == 8)) {//8/12 dédicace reportée
                    $saint = "Dedikacija";
                    $degre = 10;
                    $deg = $degr[$degre];
                } elseif (($tab_value[2] == 12)&&($tab_value[1] == 9)) {//8/12 reporté
                    $saint = "Švč. M. Marijos Nekaltasis Prasidėjimas";
                    $degre = 10;
                    $deg = $degr[$degre];
                    $feteND = 1;
                }
                    break;
                //---------------------mardi----------/promenade si le jeudi est solennité
                case 2: if ($Zcendres-2 == $debut_semaine+1) {//mardi gras
                        $ext ='<em style=\'float:right\'> Iškyla </em>';
                        $ext_deg = '_1';
//                         $temps = 1;
                } elseif ($n == $Zpaques - 4) {//mardi saint
                    $saint = '';
                } elseif ($n == $Zpaques + 3) {
                    $saint = 'Velykų oktava';
                    $degre = '6';
                    $deg = 'Šventė 2 eilė, 2 nok';
                } elseif ($n == $Zcendres + 84) {// prom Ascension
                    $ext ='<em style=\'float:right\'> Iškyla&nbsp;&nbsp;</em>';
                    $ext_deg = '_1';
                } elseif ($n == $Zpaques + 59) {// prom Fête Dieu
                    $ext ='<em style=\'float:right\'> Iškyla&nbsp;&nbsp;</em>';
                    $ext_deg = '_1';
                    }
                    break;
                //---------------------mercredi---------------------------
                case 3: if ($Zcendres == $debut_semaine+3) { //mercredi
                    // echo "<br>".($debut_semaine+3)." ".$Zcendres;
                        $saint = 'Pelenų diena';
                        $deg = 'Šiokiadienis';
                        $temps = 1;
                        $degre = 1;
                }  elseif ($n == $Zpaques - 3) {
                    $saint = ''; //ce ficher sert uniquement à désactiver le changement du jour à 10h30
                    $degre = '96';
                    $deg = $degr[1];
                }
                    break;
                //-----------------------------jeudi hors carême et avent, < 1ordre--------------
                case 4: if (($degre < 6)) {
                        // echo $s." ".($Zpaques - 3);
                        if ($n == $Zpaques - 2) {
                            $saint = 'Didysis ketvirtadienis'; //jeudi Saint
                            $degre = '97';
                            $deg = 'Šiokiadienis';
                        } elseif ($n == $Zcendres + 86) {
                            $saint = 'Kristaus dangun Žengimo iškilmė-Šeštinės'; //ascension
                            $degre = '10';
                            $deg = $degr[$degre];
                        } elseif ($n == $Zcendres + 89) {
                                $saint = 'Devintinės'; //Pentecôte
                                $degre = '10';
                                $deg = $degr[$degre];
                        } elseif ($n == $Zpaques + 61) {
                            $saint = 'Devintinės <em style=\'float:right\'> Sakram. pal.&nbsp;&nbsp;</em>'; //Fête Dieu
                            $degre = '10';
                            if(date('w', $date_ref) == $s) {
                                $salut = "1_" . $s;
                            }
                            $deg = $degr[$degre];
                        } elseif (($temps != 1)&&($temps < 4)&&($avent == 0)) {
                            $ext ='<em style=\'float:right\'> Iškyla&nbsp;&nbsp;</em>';
                            $ext_deg = '_1';
                        }
                } elseif (($degre > 5)&&($temps == 0)) {
                    //solennite du temps ordinaire, on corrige le mardi
                            $mardi = ($debut_semaine + 2);
                }
                    break;
                //-----------------------------vendredi--------------
                case 5: if ($n == $Zpaques - 1) {
                        $saint = 'Didysis penktadienis'; //vendredi Saint
                        $degre = '98';
                        $deg = $degr[1];
                } elseif ($n == $Zpaques + 69) {//Sacré-cœur
                    $saint ='Švč. Jėzaus Širdis';
                    $degre = '9';
                    $deg = $degr[$degre];
                } elseif (($tab_value[1] < 8)&&($tab_value[3] < 7)) {//1er vendredi du mois si ce n'est pas une sollennité
                    //l'activation est dans la page et sur l'écran tactile
                    $ext ='<em style=\'float:right\'> Sakram. pal.&nbsp;&nbsp;</em>';
                    if(date('w', $date_ref) == $s) {
                        $salut = "1_" . $s;
                    }
                }
                    break;
                //-----------------------------samedi--------------
                case 6: if ($n == $Zpaques) {
                        $saint = 'Didysis šestadienis'; //samedi Saint
                        $degre = '99';
                        $deg = $degr[1];
                } elseif (($n == ($Zpaques+70))&&($tab_value[3] < 3)) {//samedi 2 semaine après la Pentecôte
                    $saint = 'Nekaltoji Švč. Mergelės Marijos Širdis';
                    $degre = 3;
                    $deg = $degr[$degre];
                } elseif (($tab_value[3] < 2)&&($temps == 0)) {//mémoire facultative
                    if (($feteND != 1)&&( $avent == 0)) { //il y a eu une fête de ND dans la semaine
                        $saint = 'De sabbato';
                        $degre = 3;
                        $deg = $degr[$degre];
                    }
                }
                    break;
                }
                // echo $temps.'pppppppppppppp';
                //-----------------application des règles $temps si existant-------------------
                switch ($temps) {
                    //avent et carême
                case 1: if (($degre < 3)&&($i != 0)) {
                        $degre = 1;
                        $deg =$degr[$degre];
                        // if ($rq_saint->deg_id == 6)//supression des 1ères vêpres
                }
                    break;
                    //octave de paques
                case 2: if ($s > 2) {
                        $saint = 'Velykų oktava';
                        $degre = 3;
                        $deg = $degr[$degre];
                }
                    break;
                default : if($tab_value[4] == 3) {
                    $saint = $tab_value[0];//emporte sur le dimanche
                    $degre = 10;
                    $deg = $degr[$degre];
                    }
                }
                // echo $degre.$tab_value[4]."<br>";
                $n_fin = $n;
                if ($n > $nb_jour) {//on est en fin d'année
                    $n = $n -$nb_jour;
                }
                $degre = ($tab_value[5] !="")?$tab_value[5]:$degre;
                //Les mémoires majeurs sont transformées en mémoires minueurs pendant le carême
                if(($temps == 1) && ($degre == 2)) {
                    $degre = 1;
                    $deg = $degr[$degre];
                }
                $coul=(($tab_value[1] == date('j'))&&($tab_value[2] == date('n'))&&(date('Y', $date_ref) == date('Y')))?'style="background-color:Moccasin;font-weight:bold"':'';
                echo "<tr ".$coul."><td>".ucwords($mois_ab[$tab_value[2]])." ".$tab_value[1]." d. - ".$sem[$s]."</td>
                <td id='st_".($n-1)."'>".$saint.$ext."</td>
                <td width=90><span id='deg_".($n-1)."' style='display:none'>".$degre.$ext_deg."</span>".$deg."</td>
                </tr>";
                $s++;
                $n = $n_fin;
            }
    $n++;
}
$lien = "";
if ($_SERVER['SERVER_NAME']=='192.168.1.178') {
    $lien = "horloge/";
}
?>
</table></center>
<script>
$(document).ready(function() {
    $('table#calendrier button').on('click',function(){
        // alert($(this).html());
        if ($(this).html()== '&gt;') {
            charge('<?php echo $lien?>calendrier',<?php echo mktime(0, 0, 0, date('n', $date_ref), date('j', $date_ref), (date('Y', $date_ref))+1)?>,'affichage')
        } else if ($(this).html()== '&lt;') {
            charge('<?php echo $lien?>calendrier',<?php echo mktime(0, 0, 0, date('n', $date_ref), date('j', $date_ref), (date('Y', $date_ref))-1)?>,'affichage')
        } else if ($(this).html()== ' &gt; ') {
            charge('<?php echo $lien?>calendrier',<?php echo mktime(0, 0, 0, date('n', $date_ref), date('j', $date_ref)+7, date('Y', $date_ref))?>,'affichage')
        } else if ($(this).html()== ' &lt; ') {
            charge('<?php echo $lien?>calendrier',<?php echo mktime(0, 0, 0, date('n', $date_ref), date('j', $date_ref)-7, date('Y', $date_ref))?>,'affichage')
        } 
    });
});// alert(samedi);
var temps = <?php echo $temps?>;
<?php 
if($mardi != "") {
    echo "$('#deg_".$mardi."').html($('#deg_".$mardi."').html().substr(0,2)+1);";
    echo "$('#st_".$mardi."').append('<em style=\'float:right\'> Iškyla&nbsp;&nbsp;</em>');";
}
?>
</script>
<?php
if ($salut) {
    $tab_salut=explode('_', $salut);
    if (($tab_salut[0] == 1)&&($tab_salut[1] == date('w', $date_ref))) {
        ?>
    <script>
        $('#sa').addClass("buttonact");
        $('#sa').removeClass("buttonnc");
    </script>
    <?php
    }
}
?>
