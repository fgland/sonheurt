var jour_actif = { "00:00:00" : "Kalėdos 24_1224_0_0",
"04:59:57":"Kazimieras+Sk_0tu_8_0",          //Lever 
"05:24:57":"Kazimieras_0uw_85_0_1",      //1 son Vigiles
"05:29:57":"Kazimieras_04e_1_0",       //début Vigiles
"07:09:57":"Kazimieras_0uw_85_1",      //1 son Laudes
"07:14:57":"Kazimieras_04e_1_0",       //début Laudes
"09:44:57":"Varpas Kazimieras_400_90_0", //Volée Messe
"09:49:57":"Skambutis 1x10_03n_10_0",  //timbre Messe
"09:54:57":"Kazimieras_0uw_85_1",      //Station Messe
"09:59:57":"Kazimieras_04e_1_0",       //Entrée Messe
"12:44:57":"V: Ang: Iškilmė 1 eilė_770_194_0",    //Angelus
"12:49:57":"Skambutis 1x10_03n_10_0",  //Timbre Sexte
"12:54:57":"Kazimieras_0uw_85_1",      //2 son Sexte
"12:59:57":"Kazimieras_04e_1_0",       //Début Sexte
"17:14:57":"Varpai 3/2/1_700_146_0",   //Volée Vêpres
"17:19:57":"Skambutis 1x10_03n_10_0",  //Timbre Vêpres
"17:24:57":"Sk2+Marija_0ut_85_1",      //Station Vêpres
"17:29:57":"Kazimieras_04e_1_0",       //Début Vêpres
"18:49:57":"Skambutis_03s_21_0",       //vœux
"19:24:57":"Skambutis_03s_21_0",       //Repas
"20:29:57":"V: Ang: Iškilmė 1 eilė_770_194_0",   //Angélus
"21:44:57":"Varpai 3/2/1_700_146_0",   //volée vigiles
"21:49:57":"Skambutis 1x10_03n_10_0",  //1 son
"21:54:57":"Kazimieras_0uw_85_1",      //station
"21:59:57":"Kazimieras_04e_1_0",       //debut vigiles
"22:10:00":"changement_00o_1_0",       //-------------changement de jour
"23:59:59":"fin_000_0_3"
};
