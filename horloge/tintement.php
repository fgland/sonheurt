<?php
session_start();
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
require "lg_".$_SESSION["lg"].".php"; 
?>
<script>
$(document).ready(function(){
    $('#but button').click(function(){
        $(this).prop("disabled",true);
        $('#affichage').css('display','none');
        charge('envoi',$(this).attr('code'),'envoi');
    });
});
</script>
<div id='but'>
    <div style ="width:400px;float:right">
        <button code = "sk2+Varpas1/2/3_0xv_85"><?php echo $lg[24]?></button><br>
        <button class="buttonp" code = "Marija 1x12_01p_65"><?php echo $lg[25]?></button><button class="buttonp" code = "Marija 3x3_01m_35"><?php echo $lg[26]?></button><button class="buttonp" code = "sk2+Marija_0ut_85">sk2 + Marija</button><br>
        <button class="buttonp" code = "Petras 1x12_02p_65"><?php echo $lg[27]?></button><button class="buttonp" code = "Petras 3x3_02m_35"><?php echo $lg[28]?></button><button class="buttonp" code = "sk2+Petras_0uu_85">sk2 + Petras</button><br>
        <button class="buttonp" code = "Kazimieras 1x12_04p_65"><?php echo $lg[29]?></button><button class="buttonp" code = "Kazimieras 3x3_04m_35"><?php echo $lg[30]?></button><button class="buttonp" code = "sk2+Kazimieras_0uw_85">sk2 + Kazimieras</button><br>
        <button class="buttonp" style="float:left" code = "Skambutis 1x10_03n_10"><?php echo $lg[31]?></button>
        <button class="buttonp" style="float:left" code = "Décès et volée funèbre_0xt_840"><?php echo $lg[64]?></button>
    <button class="buttonp" style="float:right" code = "Skambutis 3x5_03s_21"><?php echo $lg[32]?></button><br>
    </div>
    <!-- <div style ="width:400px;float:right">
    <button class="buttondemi" style="float:left" code = "Skambutis 1x10_03n_10"><?php echo $lg[31]?></button>
    <button class="buttondemi" style="float:right" code = "Skambutis 3x5_03s_21"><?php echo $lg[32]?></button><br>
</div> -->
</div>
<button class='fermer' onclick="bascule();"><img src="sortie.png"></button>
<script>
$('#but button').css('height',370/5);
</script>
