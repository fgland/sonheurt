<?php
session_start();
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
require "lg_".$_SESSION["lg"].".php"; 
?>
<script>
$(document).ready(function(){
    $('#but button').click(function(){
        $(this).prop("disabled",true);
        $('#affichage').css('display','none');
        charge('envoi',$(this).attr('code'),'envoi');
    }); 
});
</script>
<div id='but'>
<button code = "Volée Solennité_700_146"><?php echo $lg[15]?></button><br>
<button code = "Volée Fêtes II_300_156"><?php echo $lg[16]?></button><br>
<button code = "Volée Fêtes III_600_156"><?php echo $lg[17]?></button><br>
<button code = "Volée Dimanche_500_156"><?php echo $lg[18]?></button><br>
<div style ="width:400px;float:right">
    <button class="buttondemi" style="float:right" code = "Varpas Marija_0vt_120"><?php echo $lg[19]?></button>
    <button class="buttondemi" style="float:left" code = "Varpas Marija_0wt_60"><?php echo $lg[20]?></button>
    <button class="buttonp" code = "Volée sib_400_90"><?php echo $lg[21]?></button>
    <button class="buttonp" code = "Volée sol_200_72"><?php echo $lg[22]?></button>
    <button class="buttonp" code = "Volée mib_100_60"><?php echo $lg[23]?></button></div>
</div>
<button class='fermer' onclick="bascule();"><img src="sortie.png"></button>
<script>
$('#but button').css('height',370/6);
</script>