<?php
// "17:44:57":"Varpas Kazimieras_400_90_1",&ac=0&lig=18
$ac = filter_input(INPUT_GET, "ac", FILTER_SANITIZE_STRING); //action
$port = filter_input(INPUT_GET, "port", FILTER_SANITIZE_STRING); //portée de l'action 0=jour, 1=permanent
$lig = filter_input(INPUT_GET, "lig", FILTER_SANITIZE_STRING); //n° de ligne
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING); //contenu de la ligne
print_r($_GET);
//fichier par défaut, si l'action est permanente, le n° du fichier est dans le code sonnerie
$fichier = fopen("/var/www/html/horloge/js/journee.js", "r+");
date_default_timezone_set('Europe/Vilnius');
//lecture du fichier
//3 actions :
//-- modification du jour ou permanente ac==heuson
//-- suppression au jour ou permanent   ac==sup
//-- ajout au jour ou permanent         ac==ajout
$n = 1;
$contenu = '';
if ($fichier) {
    while (($buffer = fgets($fichier, 4096)) !== false) {
        // echo $buffer . "<br>";

        if ($n == 1) {
            echo  $code_jour = $buffer; //la première ligne contient : 
            //                               nom du programme_code_promenade
            //var jour_actif = { "00:00:00" : "Iškilmė I eilė_0010_0", 
        }
        if ($n < $lig) {
            $contenu .= $buffer;
        }
        if ($n == $lig) {
            if ($ac == "heuson") {
                $contenu .=  $req . "//" . date('j-m-Y H:i:s') . "\n";
            } elseif ($ac == "sup") {
                //on ne fait rien, ce qui supprime la ligne
            } elseif ($ac == "ajout") {
                $contenu .= $buffer;
                $contenu .=  $req . "//" . date('j-m-Y H:i:s') . "\n";
            }
        }
        if ($n > $lig) {
            $contenu .= $buffer;
        }
        $n++;
    }
    if (!feof($fichier)) {
        echo "Erreur: Erreur de lecture du fichier\n";
    }
}
fclose($fichier);
//fin lecture du fichier

//écriture  du fichier journalier
$fichier = fopen("/var/www/html/horloge/js/journee.js", "w+");
fwrite($fichier,  str_replace("&#34;", '"', $contenu));
fclose($fichier);

//écriture  du fichier permanent
if ($port == 2) {
    // var jour_actif = { "00:00:00" : "Eilinė diena, iškyla._0001_0", 
    echo $ext = (substr($code_jour, -4, -3) == 1) ? '_promenade' : '';
    echo $fich = (substr($code_jour, -9, -5) * 1);
    include "programme.inc.php";

    // echo "----" . $fichierp;
    $fichier = fopen($fichierp, "w+");
    fwrite($fichier,  str_replace("&#34;", '"', $contenu));
    fclose($fichier);
}
?>

<script>window.location.reload();</script>