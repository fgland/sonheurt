<?php
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$handle = @fopen($incpath . "js/journee.js", "r");
$n = 1;
if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
        //on récupère la ligne à corriger et on décortique
        if ($n == 1) {
            // echo $buffer."<br>";
            $tab_ligne = explode("_", $buffer);
            $fich =  ($tab_ligne[2]*1);
            $ext = (substr($tab_ligne[3],0,1) == 1) ? '_promenade' : '';
        }

        if ($n == $req) {
            // echo $buffer."<br>";
            //
            // echo substr($buffer,1,8)."<br>";
            $h = substr($buffer, 1, 2);
            $m = substr($buffer, 4, 2);
            $s = substr($buffer, 7, 2);
            $tab_ligne = explode(",", substr($buffer, 11));
            $sonnerie = substr($tab_ligne[0], 1, -1); //avec le code de retransmission
            $tab_sonnerie = explode('_', $sonnerie);
        }
        $n++;
    }
    if (!feof($handle)) {
        echo "Erreur: fgets() a échoué\n";
    }
    fclose($handle);
}
//comparaison de la taille du fichier de base et de journée
//si différent, on désactive la modification permanente
//$fich = code du fichier
require "programme.inc.php";
$permanent = filesize($fichierp);
$jour = filesize("js/journee.js");
$desactiverp =($permanent != $jour)?"disabled":"";
// echo $buffer; exit;
$desactiver = '';
if ($tab_sonnerie[3] == 1) {
    $trans = 'oui';
} else {
    if ($tab_sonnerie[3] > 1) {
        //desactivation du changement de sonnerie
        $desactiver = ' disabled';
    }
    $trans = 'non';
}
?>
<style>
    .h {
        width: 30px;
    }

    .choix {
        display: none;
    }

    .choix button {
        width: 42px;
        height: 40px;
        margin: 5px;
    }

    .buttonmodif {
        width: 200px;
        height: 80px;
        border-radius: 5px;
        font-weight: bold;
    }

    #choix_o button {
        float: right;
        height: 80px;
        width: 399px;
        font-weight: bold;
        border-radius: 10px;
    }
</style>
<script>
    function valid(id) {
        parametres = "\"" + $('#h').html() + ":" + $('#m').html() + ":" + $('#s').html() + "\":\"" + $('#o').attr('code') + "_" + $('#t').attr('alt') + "\",";
        charge('valid', parametres + "&port=" + id + "&lig=<?php echo $req ?>&ac=" + $('#vide').attr('alt'), 'but');
    }
    var chx;
    $(document).ready(function() {
        $('#param button').click(function() {
            $('.choix').css('display', 'none');
            switch ($(this).attr('id')) {
                case 'h':
                    $(this).css('fontWeight', 'bold');
                    $('#choix_h').css('display', 'block');
                    $('#but').remove();
                    chx = h;
                    break;
                case 'm':
                    $(this).css('fontWeight', 'bold');
                    $('#h').css('fontWeight', 'normal');
                    $('#choix_ms').css('display', 'block');
                    $('#but').remove();
                    chx = m;
                    break;
                case 's':
                    $('#h').css('fontWeight', 'normal');
                    $('#m').css('fontWeight', 'normal');
                    $(this).css('fontWeight', 'bold');
                    $('#choix_ms').css('display', 'block');
                    $('#but').remove();
                    chx = s;
                    break;
                case 'o':
                    $('#h').css('fontWeight', 'normal');
                    $('#m').css('fontWeight', 'normal');
                    $(this).css('fontWeight', 'bold');
                    $('#choix_o').css('display', 'block');
                    $('#but').remove();
                    chx = o;
                    break;
                case 't':
                    $('#but').remove();
                    if ($(this).html() == 'oui') {
                        $(this).html('non');
                        $(this).attr('alt', 0);
                    } else {
                        $(this).html('oui');
                        $(this).attr('alt', 1);
                    }
                    $('#h').css('fontWeight', 'normal');
                    $('#m').css('fontWeight', 'normal');
                    $(this).css('fontWeight', 'bold');
                    $('#envoi').append(validation);
                    chx = t;
                    break;
            }
        });

    });

    $('#but button').css('height', 370 / 4);
    if ($('#mdheure').html() < $('#info').html()) {
        $('#heure').prop("disabled", true);
        $('#heuson').prop("disabled", true);
    }
    $('.choix button').click(function() {
        if ($(this).attr('id') == undefined) {
            $(chx).html($(this).html());
            $(this).parent().css('display', 'none');
            $('#envoi').append(validation);

        } else {
            charge('cloches', $(this).attr('id'), 'choix_o');
        }
    });
    if ($('#vide').attr('alt') == 'heuson') {
        var validation = '<div id="but"><button onclick=\'valid(1)\'>Validation journalière</button><button <?php echo $desactiverp?> onclick=\'valid(2)\'>Validation permanente</button></div>';
    } else if ($('#vide').attr('alt') == 'sup') {
        $('#envoi').append('<div id="but"><button onclick=\'valid(1)\'>Suppression journalière</button><br><br><button <?php echo $desactiverp?> onclick=\'valid(2)\'>Suppression permanente</button><br><br><button  style=\'float:right\' onclick=\'bascule()\'>Annuler</button></div>');
        $('#param button').prop("disabled",true);
    } else if ($('#vide').attr('alt') == 'ajout') {
        var validation = '<div id="but"><button onclick=\'valid(1)\'>Ajout journalier</button><br><br><button <?php echo $desactiverp?> onclick=\'valid(2)\'>Ajout permanent</button><br><br><button  style=\'float:right\' onclick=\'bascule()\'>Annuler</button></div>';
    }
</script>
<table id='param'>
    <tr>
        <th colspan="3">Heure</th>
        <th>Sonnerie</th>
        <th>Retransmission</th>
    </tr>
    <tr>
        <td><button class="h" id='h'><?php echo $h ?></button></td>
        <td><button id='m' class="h"><?php echo $m ?></td>
        <td><button id='s' class="h"><?php echo $s ?></td>
        <td><button id='o' <?php echo $desactiver?> style='width:300px' code='<?php echo substr($sonnerie, 0, -2) ?>'><?php echo $tab_sonnerie[0] ?></button></td>
        <td><button id='t' alt='<?php echo $tab_sonnerie[3] ?>'><?php echo $trans ?></button></td>
    </tr>
</table>

<div id='choix_h' class='choix'>
    <?php
    $bth = '';
    $hi = 1;
    for ($i = 1; $i < 24; $i++) {
        // $desact = ($i < ($h - 1)) ? ' disabled' : '';
        // if ($h > "18:00:55") {
        //     $desact = '';
        // }
        if ($hi < 4) {
            $fin = "";
        } else {
            $hi = 0;
            $fin = '<br>';
        }
        $hi++;
        $bth .= "<button>" . str_pad($i, 2, 0, STR_PAD_LEFT) . "</button>" . $fin;
    }
    echo $bth;
    ?>
</div>
<div id='choix_ms' class='choix'>
    <?php
    $hi = 1;
    $btms = '';
    for ($i = 1; $i < 60; $i++) {
        if ($hi < 11) {
            $fin = "";
        } else {
            $hi = 0;
            $fin = '<br>';
        }
        $hi++;
        $btms .= "<button>" . str_pad($i, 2, 0, STR_PAD_LEFT) . "</button>" . $fin;
    }
    echo $btms;
    ?>
</div>
<div id='choix_o' class='but choix'>
    <button id='tin'>Tintements</button>
    <button id='vol'>Volées</button>
    <button id='ang'>Angélus</button>
    <button id='div'>Divers</button>
</div>
<script>
    $('#choix_o button').css('height', 250 / 4);
    
</script>