<?php
session_start();
require "lg_".$_SESSION["lg"].".php"; 
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
require "lg_".$_SESSION["lg"].".php"; 
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
if ($req !="") {
    ?>
    <script>
    charge('chargement', '<?php echo $req?>', 'affichage');
    </script>
    <?php
    exit;
}
?>
<script>
$(document).ready(function(){
    $('#but button').click(function() {
        if ($(this).attr('code') == "prom") {
            $( "#prom" ).toggleClass( 'Prom');
            if($(this).hasClass('Prom')) {
                $(this).css('backgroundColor','red');
                $('#dimanche').prop("disabled",true);
            } else {
                $(this).css('backgroundColor','');
                $('#dimanche').prop("disabled",false);
            }
        } else {
            if ($('#prom').hasClass('Prom')) {
                ncode = $(this).attr('code')+'1';
            } else if ($('#modif').hasClass('Prom')) {
                ncode = $(this).attr('code')+'1';
            } else {
                ncode = $(this).attr('code')+'0';
           }
            charge('chargement',ncode,'envoi');
        }
    });
});
</script>
<div style ="width:400px;float:right" id='but'>
<button id = 'prom' code = "prom"><?php echo $lg[49]?></button>
<button class='buttondemi' style="float:left;margin-top:30px" code = '1_'><?php echo $lg[50]?></button>
<button class='buttondemi' style="float:right;margin-top:30px" code = '2_'><?php echo $lg[51]?></button>

<button class='buttondemi' style="float:left" code = '4_'><?php echo $lg[53]?></button>
<button class='buttondemi' style="float:right" code = '5_'><?php echo $lg[54]?></button>
<button class='buttondemi' style="float:left" code = '3_'><?php echo $lg[52]?></button>
<button id = 'dimanche' class='buttondemi' style="float:right" code = '8_'><?php echo $lg[55]?></button>
<button class='buttondemi' style="float:left" code = '6_'><?php echo $lg[56]?></button>
<button class='buttondemi' style="float:right" code = '7_'><?php echo $lg[57]?></button>
<br>
<button class='buttondemi' style="float:left" code = '9_'><?php echo $lg[58]?></button>
<button class='buttondemi' style="float:right" code = '10_'><?php echo $lg[59]?></button>
</div>
<button class='fermer' onclick="bascule();"><img src="sortie.png"></button>
<script>
$('#but button').css('height',370/7);
</script>