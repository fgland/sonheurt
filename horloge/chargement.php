<?php
session_start();
$p = preg_split("[/]", $_SERVER['PHP_SELF']);
$incpath = "";
for ($i = 1;$i<sizeof($p)-1;$i++) {
    $incpath = '../'.$incpath;
}
unset($p, $i);

#require 'definitions.php';
#require $incpath."mysql/connect.php";
#connexobjet();
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
$tab_req = explode('_', $req);
$jour = $tab_req[0];
$option = $tab_req[1];

/*
Le chargement de la journée suivante se fait à 10h30 à 18h55(si le jour suivant est une solennité) pour les jours ordinaire
, cela permet le contrôle avant le repas
Conditions qui sont traitées dans les fichiers index.php et calendrier.php
    en temps ordinaire
    jour suivant -| jeudi -> promenade
                  | vendredi ->adoration si 1 vendredi du mois
                  | samedi -> |angelus du dimanche
                              | s'il n'y a pas eu de fête de Notre Dame, Marija
                  | dimanche -> adoration

                  | si suivant+1 est 1er ordre, angelus 1er ordre
Les jours spécifiques ont un fichier propre nommé moisjour ex : 0325 pour l'Annonciation
Valeurs pour la première ligne
$option :   1 = promenade

pour les autres heures
            1 = retransmission
            2 = desactiver suppression (début du jour)
            3 = desactiver suppression et ajouter après (fin de la journée)
*/
if ($option == 1) {
    $ext ='_promenade';
}
// |      1 | Šiokiadienis  |        1 |
// |      2 | Mažasis min.  |        1 |
// |      3 | Didysis min.  |        1 |
// |      4 | Marija        |        1 |
// |      5 | Šventė 3      |        1 |
// |      6 | Šventė 2      |        1 |
// |      7 | Sekmadienis   |        2 |
// |      8 | Iškilmė II    |        2 |
// |      9 | Iškilmė 1     |        2 |


switch ($jour) {
case 1: $fichier ="js/ferie".$ext.".js";
    break;
case 2: $fichier ="js/memoire".$ext.".js";
    break;
case 3: $fichier ="js/maria".$ext.".js";
    break;
case 4: $fichier ="js/feteIII2".$ext.".js";
    break;
case 5: $fichier ="js/feteIII3".$ext.".js";
    break;
case 6: $fichier ="js/feteII2".$ext.".js";
    break;
case 7: $fichier ="js/feteII3".$ext.".js";
    break;
case 8: $fichier ="js/dimanche".$ext.".js";
    break;
case 9: $fichier ="js/solII".$ext.".js";
    break;
case 10: $fichier ="js/solI".$ext.".js";
    break;
case 11: $fichier ="js/dec".$ext.".js";
    break;
default: $fichier ="js/".$jour.$ext.".js";
}
$file = $fichier;
$newfile = 'js/journee.js';
// exit;
if (!copy($file, $newfile)) {
    echo "La copie $file du fichier a échoué...\n";
} else {
    ?>
    <script> window.location.reload();</script>
    <?php
}
