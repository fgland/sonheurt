<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
$tab_req = explode('_', $req);
print_r($tab_req);
$nom = $tab_req[0];
$code = $tab_req[1];

if (isset($tab_req[3])) {
    $stream = $tab_req[3];    
} else {
    $stream = 0;
}
$duree = ($tab_req[2]*1000);
if ((!$code)||($code == '000')) {
    header('location:calendrier.php');
    exit;
}
if ($req == 'undefined') {
    header('location:calendrier.php');
    exit;
}
if ($stream == 1) { //démarrage du streaming et de la sono
    charge('gestion/dem_auto', '', 'vide');
    ?>
    <script>$('#dem').css('backgroundColor','red');</script>
    <?php
}
// exit;
require $incpath.'PhpSerial.php';
$comPort = "/dev/ttyACM0"; //The com port address. This is a debian address

if (isset($req)) {
    $serial = new phpSerial;
    $serial->deviceSet($comPort);
    $serial->confBaudRate(9600);
    $serial->confParity("none");
    $serial->confCharacterLength(8);
    $serial->confStopBits(1);
    $serial->deviceOpen();
    sleep(3); //Unfortunately this is nessesary, arduino requires a 2 second delay in order to receive the message
    $serial->sendMessage($code);
    // var_dump($serial->readPort());
    $serial->deviceClose();
}

if ($tab_req[2] > 60) {
    $dure1 = sprintf("%01.00f", ($tab_req[2]/60))." mn ".($tab_req[2] % 60)." s";
} else {
    $dure1 = $tab_req[2]."s";
}
if ($code == '0wt') {
    echo "<h1><img src='attention.png'> Important.</h1>
    <h3> Vous ne devez pas utiliser la touche
    de tintement du mib(Marija) pendant cette volée réservée au glas</h3>";
    echo "<h2>Elle s'arrêtera dans ".$dure1."</h2>";
    ?>
    <script>
    setTimeout(function(){
        $('#envoi').css('display','none');
        $('#affichage').css('display','block');
        }, <?php echo $duree?>);    
    </script>
    <?php
    exit;
}

?>
<h2>Šiuo metu varpas skambina: <?php echo $nom?></h2> 
Durée :   <span id='d'><?php echo $dure1?></span>
<script>
    setTimeout(function(){
        $('#envoi').css('display','none');
        $('#affichage').css('display','block');
        }, <?php echo $duree?>);    
</script>
