<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$date = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
if (!$date) {
    $date_ref=date('U');
} else {
    $date_ref=$date;
}
?>
<script type="text/javascript" src="/js/jquery.js"></script>    
<script type="text/javascript" src="/js/horloge.js"></script>    
<link rel='stylesheet' type="text/css" href='gestion.css'>
<h3>Année liturgique</h3>
<?php
if (date('L', $date_ref) == 1) {
    $champ = "ann_id";//année bissextille
    $plus = "B";
    $nb_jour = 366;
} else {
    $champ = "ann";//année normale
    $plus = "";
    $nb_jour = 365;
}
$n = 0;
echo "<table><tr><th>N° jour</th><th>Jour</th><th>Mois</th><th>Saint</th><th>Degré</th><th>État</th><th>Fichier</th></tr>";
require "../annee".$plus.".php";
require "../degres_fr.php";
$mois = array("","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
foreach ($annee as $value) {
    $tab_value=explode('_', $value);
    echo "<tr><td>".$n."</td>
    <td>".$tab_value[1]."</td>
    <td>".$mois[$tab_value[2]]."</td>
    <td>".$tab_value[0]."</td>
    <td>".$degr[$tab_value[3]]."</td>
    <td>".$tab_value[4]."</td>
    <td>".$tab_value[5]."</td></tr>";
    $n++;
}
echo "</table>";
?>
