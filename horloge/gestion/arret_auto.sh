#!/bin/sh
IFS='_'
cat '/var/www/html/horloge/gestion/arret_stream.txt' |  while read -a wordarray; do
  duree=${wordarray[1]}
  sleep ${duree}
  ps -C 'darkice' --no-headers | grep -v defunct | cut -b1-5 | sudo xargs kill -9
done
