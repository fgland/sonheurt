<?php
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
$h = date('D, d M Y H:i:s')."\n";
// echo '<center><b>'.$h.'</b></center>';
if ($req == 2) {
    echo "<br><h1>Arrêt de la retransmission</h1>Veuillez patienter...";
    $fichier = fopen("/var/www/html/horloge/gestion/arret_stream.txt", "r+");
    fwrite($fichier, $h);
    fclose($fichier);
} else {
    echo "<br><h1>Démarrage de la retransmission</h1>Veuillez patienter...";
    $fichier = fopen("/var/www/html/horloge/gestion/dem_stream.txt", "r+");
    fwrite($fichier, $h);
    fclose($fichier);
}
?>

