<?php
// Šios dienos programa
// Rytojaus programa
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require 'definitions.php';
require $incpath."mysql/connect.php";
$date = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
connexobjet();
if (date('L') == 1) {
    $champ = "ann_id";//année bissextille
} else {
    $champ = "ann";//année normale
}

if (!$date) {
    $date_ref=date('U');
} else {
    $date_ref=$date;
}

//calcul du numero liturgique de la semaine
//les variable sont basé sur le n° du jour(z) dans l'année pour éviter les problèmes d'heure
//  ex : easterdate = 21/4/2019 12:00:00
$Zref = date('z', $date_ref);
$Zpaques = date('z', easter_date(date('Y', $date_ref)));
$Zcendres = ($Zpaques - 46);
$temps = 0;//temps ordinaire
$time25dec = mktime(0, 0, 0, 12, 25, date('Y', $date_ref));
$Zdec25 = date('z', $time25dec);
$Wdec25 = date('w', $time25dec);
$Zavent1 = ($Zdec25 - $Wdec25 - 21);
$Z34 = $Zavent1 -7;
$debut_semaine = ($Zref- date('w', $date_ref));
$paques = easter_date(date('Y', $date_ref));
$mardi = "";
// echo ($Zpaques + 62);
// echo "<br>".date('z',mktime(0,0,0,));
if ($debut_semaine < 0) {
    // début année
    $reprise = ($Zref-date('w', $date_ref));
    $req_saint = '(SELECT * FROM annee  
                         JOIN degres ON deg_id = ann_degre
                         WHERE ann_mois = 12 AND ann_jour >= (31 '.$reprise.'+1))
                  UNION ALL 
                        (SELECT * FROM annee 
                        JOIN degres ON deg_id = ann_degre 
                            WHERE ann_jour <= (7 '.$reprise.') AND ann_mois = 1)';
} else {
    $req_saint = 'SELECT * FROM annee 
                            JOIN degres ON deg_id = ann_degre 
                                WHERE '.$champ.' >= '.$debut_semaine.' 
                                AND '.$champ.' < '.($debut_semaine+7);
}
$r_saint = $idcom->query($req_saint);
$i = 0;
// echo "<br>".$Zref." >  ".($Zcendres +4)." ".($Zpaques - 42);
//----------------------temps de Noël et Épyphanie---------------------------
if (((date('j', $date_ref) - date('w', $date_ref)) < 6)&&(date('n', $date_ref)==1)) {
    $n_dimanche = 'Temps de Noël';
} elseif (((date('j', $date_ref) - date('w', $date_ref)) >= 6)&&
        (date('n', $date_ref)==1)&&
        (date('j', $date_ref) - date('w', $date_ref)) < 13) {
    $n_dimanche = "Temps de l'Épiphanie";
//----------------------temps ordinaire avant les Cendres---------------------------
} elseif ($Zref <  ($Zpaques - 42)) { //on laisse la semaine jusqu'au dimanche
    //le premier dimanche suit le 6 janvier. Si le 6 est un dimanche, le premier sera le 13
    $w_janvier6 = date('w', mktime(0, 0, 0, 1, 6, date('Y')));
    $dimanche1_ord = 6+(7 - $w_janvier6);
    $debutz_ordinaire = (date('z', mktime(0, 0, 0, 1, $dimanche1_ord, date('Y'))));
    $n_dimanche = (int)((($Zref - $debutz_ordinaire) / 7)+1)." eilinė savaitė";
//--------------------------carême----------------------------------------------
} elseif (($Zref >= ($Zcendres +4))&&($Zref < ($Zpaques -7))) {
    $temps = 1;//mémoires affichées mais programme ferie
    $dimanche1_car = $Zpaques - 42;
    $n_d = (int)((($Zref - $dimanche1_car) / 7)+1);
    $n_dimanche = ($n_d == 6)?"Didžioji savaitė":$n_d." gavėnios savaitė";
//-----------------------semaine sainte-------------------------------------------
} elseif (($Zref >= ($Zpaques -7))&&($Zref < $Zpaques)) {
    $temps = 1;//mémoires affichées mais programme ferie
    $dimanche1_car = $Zcendres + 4;
    $n_d = (int)((($Zref - $dimanche1_car) / 7)+1);
    $n_dimanche = ($n_d == 6)?"Didžioji savaitė":$n_d." gavėnios savaitė";
//-----------------------octave de Pâques-------------------------------------------
} elseif ($debut_semaine == date('z', $paques)) {
    $temps = 2;
    $n_dimanche ="Velykų oktava";
    
//-----------------------temps pascal-------------------------------------------
} elseif ($Zref < ($Zpaques+49)) {
    $n_d = (int)((($Zref - $Zpaques) / 7)+1);
    $n_dimanche = $n_d." Velykų savaitė";
    $temps = 3;
//------------------temps ordinaire après Pâques--------------------------------
} elseif (($Zref > ($Zpaques+48))&&($Zref< $Zavent1)) {
    //cacul du n° à partir du 1er dimanche de l'Avent
    $n_d = (int)(34-(($Z34 - $Zref) / 7));
    $temps = 0;
    $n_dimanche =$n_d.' eilinė savaitė';
//------------------temps de l'avent--------------------------------
} else {
    $n_d = (int)(($Zref - $Zavent1)/7)+1;
    $temps = 2;
    $n_dimanche =$n_d.' avent';
}
//------------------temps de l'avent--------------------------------
// echo "<br>".(date('z', date($date_ref))-date('w', date($date_ref)))." ".date('j/n/Y', date($date_ref))." ".date('w', date($date_ref));//." < ".(date('z', $paques)+50)."--".(int)(((date('z', $date_ref) - date('z', $paques)) / 7));
?>
<br><br><br>
<center><table>
    <tr><th colspan="3" style='background-color:Lavender'>
    <button style='float:left' onclick="charge('info',<?php echo mktime(0, 0, 0, date('n', $date_ref), date('j', $date_ref)-7, date('Y', $date_ref))?>,'affichage')"><</button>
    <?php echo $n_dimanche?>
    <button style='float:right' onclick="charge('info',<?php echo mktime(0, 0, 0, date('n', $date_ref), date('j', $date_ref)+7, date('Y', $date_ref))?>,'affichage')">></button>
    </th></tr>
<?php
$feteND = '';
$salut = '';
// echo $rq_saint->$champ." ".(date('z', $paques)+50);
while ($rq_saint =$r_saint->fetch_object()) {
    $degre = $rq_saint->deg_id;//    |ces valeurs
    $saint = $rq_saint->ann_saint;// |seront écrasées
    $deg = $rq_saint->deg_nom;//     |par les cas particuliers
//     $degre = $rq_saint->ann_degre;
    $ext = '';//variable nom promenade/salut
    $ext_deg = '_0';//variable promenade(1)/salut(2)/jour emportant sur le dimanche(3)
// echo $rq_saint->$champ." ".(date('z', $paques)+49)."<br>";
    if ($rq_saint->ann_etat == 1) {
        $feteND = 1;
    }
    switch ($i) {
    //-----------------------Dimanche-----------------------------------
    case 0: if ($rq_saint->ann_etat == 2) {//fête l'emportant sur le dimanche
        $saint = $rq_saint->ann_saint;
//         $deg = '';
    } elseif ((date('j', $date_ref)== 6)&&(date('n', $date_ref) == 1)) {//Baptème
        $saint = 'Kristaus apsireiškimas';
        $degre = 8;
        $deg = 'Sekmadienis';
    } elseif (((date('j', $date_ref)- date('w', $date_ref)) <= 13)&&((date('j', $date_ref)- date('w', $date_ref)) > 6)&&($rq_saint->ann_mois == 1)) {//Baptème
        $saint = 'Kristaus apsireiškimas';
        $degre = 6;
        $deg = 'Sekmadienis';
    } elseif ($rq_saint->$champ == $Zpaques) {//Pâques
        $saint = 'Velykų';
        $degre = 100;
        $deg = 'Iškilmė 1';
    } elseif ($rq_saint->$champ == $Zpaques+7) {//Pâques
        $saint = 'Atvelykis';
        $degre = 9;
        $deg = 'Iškilmė 2';
    } elseif ($rq_saint->$champ == ($Zpaques+49)) {//Pentecôte
        $saint = 'Šv. Dvasios atsiuntimas-Sekminės';
        $degre = 10;
        $deg = 'Iškilmė 1';
    } elseif ($rq_saint->$champ == ($Zpaques+56)) {//Sainte Trinité
        $saint = 'Švč. Trejybė ';
        $degre = 9;
        $deg = 'Iškilmė 2';
    } else {
        $saint = '';
        $degre = 8;
        $deg = 'Sekmadienis';
    }
        break;
    //-------------------lundi----------------
    case 1: if ($rq_saint->$champ == $Zpaques + 1) {
            $saint = 'Velykų II diena';
            $degre = '101';
            $deg = 'Iškilmė 2';
    } elseif ($rq_saint->$champ == $Zpaques+50) {
        $saint = "Švč. Mergelė Marija Bažničios Motina";
        $degre = '3';
        $deg = 'Marija';
        $feteND = 1;
    }
        break;
    //---------------------mardi----------/promenade si le jeudi est solennité
    case 2: if ($Zcendres-2 == $debut_semaine+1) {//mardi gras
            $ext ='<em> Iškyla.</em>';
            $ext_deg = '_1';
    } elseif ($rq_saint->$champ == $Zpaques + 2) {
        $saint = 'Velykų oktava';
        $degre = '6';
        $deg = 'Šventė 2 eilė, 2 nok';
    } elseif ($rq_saint->$champ == $Zcendres + 83) {// prom Ascension
        $ext ='<em> Iškyla</em>';
        $ext_deg = '_1';
    } elseif ($rq_saint->$champ == $Zpaques + 58) {// prom Fête Dieu
        $ext ='<em> Iškyla</em>';
        $ext_deg = '_1';
     }
        break;
    //---------------------mercredi---------------------------
    case 3: if ($Zcendres == $debut_semaine+3) { //mercredi
        // echo "<br>".($debut_semaine+3)." ".$Zcendres;
            $saint = 'Pelenų diena';
            $deg = 'Šiokiadienis';
            $temps = 1;
            $degre = 1;
    } elseif ($rq_saint->$champ == $Zpaques - 4) {
        $saint = ''; //ce ficher sert uniquement à désactiver le changement du jour à 10h30
        $degre = '96';
        $deg = 'Šiokiadienis';
    }
        break;
    //-----------------------------jeudi hors carême et avent, < 1ordre--------------
    case 4: if (($degre < 8)) {
            // echo $rq_saint->$champ." ".($Zpaques - 3);
            if ($rq_saint->$champ == $Zpaques - 3) {
                $saint = 'Didysis ketvirtadienis'; //jeudi Saint
                $degre = '97';
                $deg = 'Šiokiadienis';
            } elseif ($rq_saint->$champ == $Zcendres + 85) {
                $saint = 'Kristaus dangun Žengimo iškilmė-Šeštinės'; //ascension
                $degre = '10';
                $deg = 'Iškilmė 1';
            } elseif ($rq_saint->$champ == $Zcendres + 89) {
                    $saint = 'Devintinės'; //Pentecôte
                    $degre = '10';
                    $deg = 'Iškilmė 1';
            } elseif ($rq_saint->$champ == $Zpaques + 60) {
                $saint = 'Devintinės, <em> Sakram. pal.</em>'; //Fête Dieu
                $degre = '10';
                $salut = "1_".($Zpaques + 60);
                $deg = 'Iškilmė 1';
            } elseif ($temps != 1) {
                $ext ='<em> Iškyla</em>';
                $ext_deg = '_1';
            }
    } elseif (($degre > 8)&&($temps == 0)) {
        //solennite du temps ordinaire, on corrige le mardi
        $mardi = ($rq_saint->$champ - 2);
    }
        break;
    //-----------------------------vendredi--------------
    case 5: if ($rq_saint->$champ == $Zpaques - 2) {
            $saint = 'Didysis penktadienis'; //vendredi Saint
            $degre = '98';
            $deg = 'Šiokiadienis';
    } elseif ($rq_saint->$champ == $Zpaques + 68) {//Sacré-cœur
        $saint ='Švč. Jėzaus Širdis';
        $degre = '9';
        $deg = 'Iškilmė II';
    } elseif (($rq_saint->ann_jour < 8)&&($rq_saint->deg_id < 9)) {//1er vendredi du mois si ce n'est pas une sollennité
        //l'activation est dans la page et sur l'écran tactile
        $ext ='<em> Sakram. pal.</em>';
        $salut = "1_".$rq_saint->$champ;
    }
        break;
    //-----------------------------samedi--------------
    case 6: if ($rq_saint->$champ == $Zpaques - 1) {
            $saint = 'Didysis šestadienis'; //samedi Saint
            $degre = '99';
            $deg = 'Šiokiadienis';
    } elseif (($rq_saint->$champ == ($Zpaques+69))&&($rq_saint->deg_id < 3)) {//samedi 2 semaine après la Pentecôte
        $saint = 'imm. Cordis b. M. V.';
        $degre = 3;
        $deg = 'Marija';
    } elseif (($rq_saint->deg_id < 3)&&($temps == 0)) {
        if ($feteND != 1) { //il y a eu une fête de ND dans la semaine
            $saint = 'De sabbato';
            $deg = 'Marija';
            $degre = 3;
        }
    }
        break;
    }
    
    //-----------------application des règles $temps si existant-------------------
    switch ($temps) {
        //avent et carême
    case 1: if (($degre < 3)&&($i != 0)) {
            $deg ='Šiokiadienis';
            $degre = 1;
            // if ($rq_saint->deg_id == 6)//supression des 1ères vêpres
    }
        break;
        //octave de paques
    case 2: if (($temps == 2)&&($i > 2)) {
            $saint = 'Velykų oktava';
            $deg = 'Marija';
            $degre = 4;
    }
        break;
    }
    $degre= ($rq_saint->ann_fichier !="")?$rq_saint->ann_fichier:$degre;
    $coul=(($rq_saint->ann_jour == date('j'))&&(($rq_saint->ann_mois == date('n'))))?'style="background-color:Moccasin;font-weight:bold"':'';
    echo "<tr ".$coul."><td>".ucwords($mois_ab[$rq_saint->ann_mois])." ".$rq_saint->ann_jour." d. - ".$sem[$i]."</td>
    <td id='st_".$rq_saint->$champ."'>".$saint.$ext."</td>
    <td width=90><span id='deg_".$rq_saint->$champ."' style='display:none'>".$degre.$ext_deg."</span>".$deg."</td>
    </tr>";
    $i++;
}
?>
</table></center>
<script>
var temps = <?php echo $temps?>;
<?php 
if($mardi != "") {
    echo "$('#deg_".$mardi."').html($('#deg_".$mardi."').html().substr(0,2)+1);";
    echo "$('#st_".$mardi."').append('<em>, Iškyla</em>');";
}
?>
</script>
<?php
if ($salut) {
    $tab_salut=explode('_', $salut);
    if (($tab_salut[0] == 1)&&($tab_salut[1] == date('z'))) {
        ?>
    <script>
        $('#sa').addClass("buttonact");
        $('#sa').removeClass("buttonnc");
    </script>
    <?php
    }
}
?>
