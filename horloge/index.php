<?php
session_start();
if (!isset($_SESSION['lg'])) {
    $_SESSION['lg'] = "lt";
}
$lg = $_SESSION['lg'];
require "lg_" . $lg . ".php";
?>
<html>
<head>
    <title><?php echo $_SERVER['SERVER_NAME'] ?></title>
    <meta charset="utf-8">
    <meta content="">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <link rel='stylesheet' type="text/css" href='horloge.css'>
    <script type="text/javascript" src="/js/journee.js"></script>
    <script type="text/javascript" src="/js/horloge.js"></script>
    <script>
        var heure = $('#info').html();
        function h_suivante(tt) {
            var heure;
            var res;
            var donnees;
            if ($('#donnees').html() != "fin 000") { //la journée n'est pas finie
                for (var h in jour_actif) { //lecture du fichier journee.js généré la veille    
                    if (h >= tt) {
                        heure = h;
                        res = jour_actif[h].split("_");
                        donnees = jour_actif[h];
                        break;
                    }
                }
                $('#info').html(heure);
                $('#donnees').html(res[0] + ' ' + res[1]);
            }
        }
        function bascule() {
            if ($('#affichage').css('display') == 'block') {
                $('#affichage').css('display', 'none');
                $('#envoi').css('display', 'block');
            } else {
                $('#envoi').css('display', 'none');
                $('#affichage').css('display', 'block');
                $('#envoi').empty();
            }
        }
        var vu = 0;
        var depassement = 0;
        samedi = '8_0';
        function horloge() {
            heure = $('#info').html();
            var tt = new Date().toLocaleTimeString(); // hh:mm:ss 
            tt = tt.substr(0, 8);
            if ((heure == '23:59:59') && (tt < '00:00:15')) { // 23:59:59
                $('#info').html('00:00:00');
                $('#donnees').html('Reprise');
            }
            <?php $compl = '' ?>
            //arret de la retransmission à 14h15
            if (tt == '14:15:00') {
                charge('gestion/arret_auto', '', 'vide');
            }
            if ((heure == tt) || (depassement == 1)) { //envoi des données et actualisation
                depassement = 0;
                code = jour_actif[heure];
                //gestion du changement de jour à 10h30 et 18:55
                res = code.split("_");
                if (res[1] == '00l') { // actualisation de l'interface
                    window.location.reload();
                } else if (res[1] == '00m') { //10h30-------------------------------------
                    //quand le jour suivant à un fichier propre : > 90
                    //vérification à 10:30:00 du jour suivant si 1er vêpres(dimanche et >)
                    // date('w') 0 (pour dimanche) à 6 (pour samedi)
                    //seulement si ann_etat != 2 (1=fête de ND, 2=emporte sur le dimanche)
                    if (<?php echo date('w') ?> == 6) { //samedi
                        if (((<?php echo date("j") ?> > 25) && (<?php echo date("j") ?> <= 31)) && (<?php echo date("n") ?> == 12)) {
                            //si on est entre le 26 et 31 décembre, le jour suivant Sainte Famille
                            charge('chargement', '9_0', 'envoi');
                        } else if (((<?php echo date("j") ?> > 6) && (<?php echo date("j") ?> < 13)) && (<?php echo date("n") ?> == 1)) {
                            //si on est entre le 6 et 14 janvier, le jour suivant Baptême du Seigneur
                            charge('chargement', '9_0', 'envoi');
                        } else if ((<?php echo date("j") ?> == 1) && (<?php echo date("n") ?> == 2)) {
                            //2 février l'emporte sur le dimanche
                            charge('chargement', '7_0', 'envoi');
                        } else if ((<?php echo date("j") ?> == 10) && (<?php echo date("n") ?> == 7)) {
                            //11 juillet l'emporte sur le dimanche
                            charge('chargement', '10_0', 'envoi');
                        } else if ((<?php echo date("j") ?> == 14) && (<?php echo date("n") ?> == 8)) {
                            //15 août l'emporte sur le dimanche
                            charge('chargement', '10_0', 'envoi');
                        } else if ((<?php echo date("n") ?> == 10) && (<?php echo date("j") ?> == 31)) { //1er vepres du  novembre
                            charge('chargement', '10_0', 'envoi');
                        } else {
                            deg=$('#deg_<?php echo(date('z'))?>').html().split("_");
                            var degre = [0];
                        }
                    } else {
                        degre_ = $('#deg_<?php echo (date('z') + 1) ?>').html();
                        degre = degre_.split("_");
                    }

                    if (degre[0] < 90) {
                        if ((<?php echo date('w') ?> == 0) && (temps == 0)) {
                            if (degre[0] > 8) {
                                charge('chargement', degre_, 'envoi');
                            }
                        } else if (<?php echo date('w') ?> == 4) { //le jeudi, promenade 
                            if (degre[0] > 8) {
                                charge('chargement', degre[0] + '_1', 'envoi');
                            }
                        } else if (<?php echo date('z') ?> == <?php echo (date('z', easter_date()) + 68) ?>) {
                            //sacré cœur, on ne fait rien
                        } else if ((<?php echo date('w') ?> == 6) && (degre[1] != 2)) { //le samedi
                            if (<?php echo date('z') ?> == <?php echo (date('z', easter_date()) + 6) ?>) {
                                //a revoir car il faudrait connaître le degré du dimanche
                                charge('chargement', '9_0', 'envoi');
                            } else if (<?php echo date('z') ?> == <?php echo (date('z', easter_date()) + 48) ?>) {
                                charge('chargement', '10_0', 'envoi'); //1er vêpres de la Pentecôte
                            } else if (<?php echo date('z') ?> == <?php echo (date('z', easter_date()) + 55) ?>) {
                                charge('chargement', '9_0', 'envoi'); //1er vêpres de la Trinité
                            } else if ((deg[0] > 8) && (temps == 0)) {
                                //si ce jour est une solennité du temps ordinaire, on ne fait
                                //rien, la solennité l'emporte que le dimanche
                            } else if (<?php echo date('m-d') ?> == '08-05') { //5 aout
                                charge('chargement', '7_0', 'envoi');
                            } else {
                                charge('chargement', samedi, 'envoi');
                            }
                        } else { //autres jours, vérification de donnée dans la page 
                            if ($('#deg_<?php echo (date('z') + 1) ?>').length == 1) {
                                degre_ = $('#deg_<?php echo (date('z') + 1) ?>').html();
                                degre = degre_.split("_");
                                if (degre[0] > 8) {
                                    if ((<?php echo date('w') ?> == 0) && (temps != 0)) {
                                        //on ne fait rien si le lundi est une solennité hors du temps ordinaire
                                    } else {
                                        //si on est jeudi, promenade
                                        if (<?php echo date('w') ?> == 4) {
                                            $compl = '_1';
                                        }
                                        charge('chargement', degre_<?php echo $compl ?>, 'envoi');
                                        bascule();
                                    }
                                }
                            }
                        }
                    } else if (degre[0] == 1102) {
                        //1 novembre, on ne charge pas le jour suivant
                    } else {
                        //on ne fête le 25 mars que s'il n'est pas compris entre le début de la semaine sainte et la fin de l'octave de Pâques
                        //à faire
                        charge('chargement', degre[0] + '_0', 'envoi');
                    }
                } else if (res[1] == '00n') { //salut à 19h10
                    //             alert('2');
                    if ($('#sa').hasClass('buttonact') == true) {
                        charge('envoi', 'Sk2+Marija_0ut_85', 'envoi');
                        bascule();
                    }
                } else if (res[1] == '00o') { //gestion du changement de jour 18:55 sauf le 23 décembre :22h15
                    //une action seulement si $('#deg_ac').html(), actualisé à 10h30, est inférieur à 8 (dimanche)
                    //                                                                           degre du jour suivant != degre de ce jour
                    if ((($('#deg_ac').html() * 1) > 7) || ($('#deg_<?php echo (date('z') + 1) ?>').html().split('_')[0] != ($('#deg_ac').html().split('_')[0] * 1))) {
                        if (<?php echo date('w') ?> == 6) { //samedi soir
                            charge('chargement', '8_0', 'envoi');
                            bascule();
                        } else {
                            //autres jours
                            degre_ = $('#deg_<?php echo (date('z') + 1) ?>').html();
                            charge('chargement', degre_, 'envoi');
                            bascule();
                        }
                    }
                } else { //déclanchement manuel
                    //seulement si le bouton M/A est enfoncé        
                    if ($('#ma').hasClass('buttonact') == true) {
                        charge('envoi', '' + code + '', 'envoi');
                        bascule();
                    }
                }
                h_suivante(tt);
                vu = 1;

            } else if (heure < tt) { //ne devrait jamais arriver qu'au rechagement de la page
                if (($('#ma').hasClass('buttonact') == true) && (heure != '00:00:00')) {
                    //l'action prévue n'a pas été réalisée lecture de l'heure passée      
                    //et envoi du code seulement si le bouton M/A est enfoncé        
                    //cacul du décalage
                    var hhh_enr = $('#info').html().split(':');
                    var hms_enr = (parseInt(hhh_enr[0] * 3600) + parseInt(hhh_enr[1] * 60) + parseInt(hhh_enr[2]));
                    var hhh_act = tt.split(':');
                    var hms_act = (parseInt(hhh_act[0] * 3600) + parseInt(hhh_act[1] * 60) + parseInt(hhh_act[2]));
                    retard = (hms_act - hms_enr);
                    if ((vu = 0) && (retard < 60)) {
                        depassement = 1; //permet le rappel de la fonction
                        horloge();
                    }
                }
                h_suivante(tt);
            }
            vu = 0;
            $('#timer').html(tt); //setTimeout
            setTimeout(horloge, 997); // mise à jour du contenu "timer" toutes les secondes
        }
        /**--------------liste du programme horaire de la journée -----------------*/
        function h_jour() {
            //liste des horaires de la journée
            //on ignore la première ligne qui contient le nom et code du programme
            var jour = '<div id="dg" class="prog">';
            var n = 1;
            var max = Math.round(Object.keys(jour_actif).length / 2);
            for (var h in jour_actif) { //lecture du fichier journee.js généré la veille    
                res = jour_actif[h].split("_");
                if (n > 1) {
                    jour += "<button fil='" + res[3] + "' lig='" + n + "'>" + h + "</button> " + res[0] + "-" + res[1] + "<br>";
                }
                if (n == max) {
                    jour += "</div><div lig='" + n + "' id='dd' class='prog'>";
                }
                n++;
            }
            jour += "</div>";
            $('#affichage').css('display', 'none');
            $('#envoi').css('display', 'block');
            $('#envoi').html(jour);
            $(".prog button").click(function() {
                /* clic sur bouton propose 4 options pour des actions temporaires : 
                appliquer : choisir cette heure comme prochaine action
                modifier : modifier l'heure, la sonnerie et la retransmission
                supprimer : retire l'action du tableau
                ajouter une heure
                ;
                */
                charge('modif', $(this).html()+'&lig='+$(this).attr('lig')+'&fil='+$(this).attr('fil'), 'envoi');
            });
        }

        $(document).ready(function() {
            charge('calendrier', '', 'affichage');
            res = jour_actif["00:00:00"].split("_");
            $('#progr').html(res[0]);
            $('#deg_ac').html(res[1]);
            $('#but button').click(function() {
                charge('programmes', $(this).attr('code'), 'affichage');
            });
            $("button.toggler").click(function() {
                if ($(this).hasClass("buttonnc") == false) {
                    $(this).removeClass("buttonact");
                    $(this).addClass("buttonnc");
                } else {
                    $(this).addClass("buttonact");
                    $(this).removeClass("buttonnc");
                }
            });
            <?php

            if (($_SERVER['REMOTE_ADDR'] != '127.0.0.1') && ($_SERVER['REMOTE_ADDR'] != '192.168.1.178')) {
            ?>
                $('body').css('backgroundColor', 'orange');
                $('#ma').removeClass("buttonact");
                $('#ma').addClass("buttonnc");
                $('#progr').append(' <?php echo $_SERVER['REMOTE_ADDR'] ?>');
                // setTimeout(function(){window.location.reload(),100000});
            <?php
            }
            if ((date('w') == 0) && (($_SERVER['REMOTE_ADDR'] == '127.0.0.1') || ($_SERVER['REMOTE_ADDR'] == '192.168.1.178'))) { //dimanche, salut
            ?>
                $('#sa').removeClass("buttonnc");
                $('#sa').addClass("buttonact");
            <?php
            }
            ?>
        });
    </script>
</head>
<body onload="horloge();">
    <center>
        <button id='sa' class='toggler buttonnc' style='float:left'>Sak. pal.</button>
        <button id='dem' class='buttonnc' onclick="bascule();charge('retransmission','','envoi')" style='float:left'></button>
        <button id='ma' class='toggler buttonact' style='float:right'>&nbsp;Įjungta<br>Išjungta</button>
        <div id="timer"></div>
        <div>Šios dienos programa : <b></b><span class='g' id='progr'></span></b><span style='display:none' id='deg_ac'></span><br> sekantis skambutis : <span id='donnees'> </span> <button onclick="h_jour()" class='g' id='info'>00:00:00</button>
        </div>
        </center>
    <div id='conteneur'>
        <div id="menu">
            <div id='vide' alt='1'></div>
            <button onclick="$('#affichage').css('display','none');$('#envoi').css('display','block');charge('angelus','','envoi')">V. Ang.</button>
            <button onclick="bascule();charge('volees','','envoi')">Varpai</button>
            <button onclick="bascule();charge('tintement','','envoi')">Skambutis</button>
            <button id='plus' onclick="bascule();charge('autres','','envoi')">...</button>
        </div>
        <div id='affichage'></div>
        <div id='envoi'></div>
    </div>
</body>
</html>
