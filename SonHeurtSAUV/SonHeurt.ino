/*
  Programmation des sonnerie de cloches journalière
  --------------------en service-------------------------------
  Ce code contient le cycle de déclanchement des cloches et timbres
  en fonction de la commande envoyée par le rasperry qui gère le programme
  la journée

  commencé le 27 décembre 2018 suite à une panne du 24 décembre
  mise en service semi automatique le 15 janvier 2019
  mise en place du raspberry de contrôle le 3 février 2019
  mise en service automatique le 6 février 2019

  3 valeurs envoyés en paramètre en dessous de 116 : Volée, Tintement, options

  Les trois chiffres sont indispensables pour déclencher l'action
  un contrôle des données reçues, ligne 469 réinitialise les variables s'il manque un code

  les trois valeurs obligatoires
    xx0 = angelus
    x00 = volée
    0xx = tintements+options ou clavier virtuel

  Numéro des cloches
    Volée, de 1 à 7, somme des numéros des cloches à lancer
      0 = ''
      1 = Mib
      2 = Sol
      4 = Sib
    Tintements, de 0 à 4, seul l'angélus 1er ordre utilise les 3 cloches
      0 = ''
      1 = Mib
      2 = Sol
      3 = Timbre
      4 = Sib
      5 = 3 cloches
    nbCoup
     101 : c_p = 1; break;(e)
     105 : c_p = 5; break;(i)
     110 : c_p = 10; break;(n)
     112 : c_p = 12; break;(p)
     109 : c_p = 9; divseur = 3; break;(m) angelus, pause5s entre coup, 10 entre groupes
     115 : c_p = 15;
     Tout nombre entre 101 et 115 est accepté. Si le nombre donné est divisible par 3 ou 5, il y aura pause

  Les variables série reçue > 115 sont un groupe de sonneries correspondant à un poussoir virtuel

  les programmes     ligne
    setup             89
    volee            111
    titementAngelus  180
    angelus          229
    tintements       239
    poussoir         329
    loop             402
*/
// Sorties--------------------------
//               mi  sol    si
int tabVolee[5] {16, 17, 0, 18};
//                           mi    sol        si
long int tabTempsvolee[5] {60000, 72000, 0, 90000};
const int enCours =  13;
//                   mi  sol ti  si
int tabTintement[5] {11, 12, 14, 15};
//entrees-------------------------------
//10 boutons sur l'entrée analogique A5 (19)
int sensorPin = A5;
int sensorValue = 0;
int rc = 248;//256;//valeur de sensorValue boutons relachés, a adapter en focntion de la carte
//les broches 2 et3 sont réservés pour d'éventuelles interruptions externes

// durée d'impulsion  mi  sol   ti   si
int tabImpulsion[5] {250, 250, 470, 250}; //récupéré avec [t]
int tabCoup[5] {10000, 10000, 10000, 10000}; //vitesse maximum des tintements manuels
int temps;
int tempo1;           //variable en fonction de la cloche
int tempo2 = 500;     //1/2 seconde
int k = 1;            //compteur de la réception série
int l = 1;            //controle de trois donées reçues
int tabRC[3];         //conteneur des valeurs reçues
int n = 0;            //compteur général
int commandeRecue = 0;//données séries
int poussoir = 0;     //numéro du bouton détecté, remis à zéro à chaque fin de traitement
int cloche;           //utilisé dans le clavier virtuel
unsigned long previousMillis = 0;//sera utilisé pour la volée non bloquante de Mib(glas)
const long interval = tabTempsvolee[0];
int sortie = 0;
int sensorBase;
//-------------------------------------------------------------

void setup() {
  Serial.begin (9600);
  //entree : 10 poussoirs sur une entrée analogique
  pinMode (sensorPin, INPUT_PULLUP);//(19)
  sensorBase = analogRead(sensorPin);//valeur de référence du clavier
  Serial.print(sensorBase);
  //  les sorties
  //3 voyants poussoirs(8-10),3 volées(16-18),timtements(11, 12, 14, 15),M/A(13)
  //les entrées
  //4 sert à doubler le clavier
  //les entrées 5-7 sont reliées à des monostables externes déclenchées par la fin de la volée
  //elles empêchent le tintement de la cloche dont la volée vient de s'arrêter
  //19, entée analogique clavier
  for (int i = 4; i < 19; i++) {
    if (i < 8) {
      pinMode(i, INPUT);
    } else {
      pinMode(i, OUTPUT);
    }
    digitalWrite(i, LOW);
  }
}
//  la volée------------------------------------------
void volee(int v) {
  int npin = v - 49;
  switch (v) {
    // (49)-1     maria(samedi et fête de Marie), tintement mi, 0,5,10-20,25,30-40-45-50-pause 5s-volée
    // (50)-2     mémoire, tintement sol, 0,5,10-20,25,30-40-45-50-pause 5s-volée
    // (51)-2-1(3)   2_ordre, tintement mi, 0,5,10-20,25,30-40-45-50-pause 5s-volée
    // (52)-4     férie, tintement si, 0,4,8-16,20,24-32-36-40-pause 10s-volée
    // (53)-4-1(5)   dimanche, tintement mi, 0,5,10-20,25,30-40-45-50-pause 5s-volée
    // (54)-4-2(6)   3_ordre, tintement sol , 0,5,10-20,25,30-40-45-50-pause 5s-volée (53)
    // (55)-4-2-1(7) 1_ordre, tintement si/sol/mi, 0,4,8-20,25,30-40-45-50-pause 5s-volée
    //tabVolee[5] {16, 17, 0, 18};
    //tabTempsvolee[5] {60000, 72000, 0, 90000};
    case 49 : //Maria, samedi, fête de Marie
      digitalWrite(tabVolee[npin], HIGH);//mi
      delay(tabTempsvolee[npin]);
      digitalWrite(tabVolee[npin], LOW);
      break;
    case 50 : //Mémoire, sol
      digitalWrite(tabVolee[npin], HIGH);
      delay(tabTempsvolee[npin]);
      digitalWrite(tabVolee[npin], LOW);
      break;
    case 51 : //2 ordre, cloches 2 et 1
      digitalWrite(tabVolee[1], HIGH);// sol |----------136---------|-20-\_
      delay(14000);//                    mi  |     |--------122-----\______156
      digitalWrite(tabVolee[0], HIGH);//     0     14              136
      delay(122000);
      digitalWrite(tabVolee[0], LOW);
      delay(20000);
      digitalWrite(tabVolee[1], LOW);
      break;
    case 52 : //Férie, si
      digitalWrite(tabVolee[npin], HIGH);
      delay(tabTempsvolee[npin]);
      digitalWrite(tabVolee[npin], LOW);
      break;
    case 53 : //Dimanche, cloches 4 et 1
      digitalWrite(tabVolee[3], HIGH);//   si |----10---|---------------|-6-\_
      delay(10000);//                      mi |         /------140------\_____156
      digitalWrite(tabVolee[0], HIGH);//      0         10             150
      delay(140000);
      digitalWrite(tabVolee[0], LOW);
      delay(6000);
      digitalWrite(tabVolee[3], LOW);
      break;
    case 54 : //cloches 4 et 2
      digitalWrite(tabVolee[3], HIGH);// si |--10---|---------132------|-16-\____
      delay(10000);//                    sol|       /--------132-------\_________156
      digitalWrite(tabVolee[1], HIGH);
      delay(132000);
      digitalWrite(tabVolee[1], LOW);
      delay(16000);
      digitalWrite(tabVolee[3], LOW);
      break;
    case 55 : //1 ordre, cloches 4, 2 et 1
      digitalWrite(tabVolee[3], HIGH);//si /---10---|-------|-----------------|------\________
      delay(10000);//                   sol_________/--10---|-----------------|--10--|--6--\__
      digitalWrite(tabVolee[1], HIGH);//mi_________________/--------110--------\_______________146
      delay(10000);                   //  (10000)   (10000)      (100000)      (10000)(6000)
      digitalWrite(tabVolee[0], HIGH);//
      delay(100000);
      digitalWrite(tabVolee[0], LOW);
      delay(10000);
      digitalWrite(tabVolee[1], LOW);
      delay(6000);
      digitalWrite(tabVolee[3], LOW);
      break;
  }
}
void tintementAngelus(int c) { //numero de la cloche
  //                 mi  sol ti  si
  //tabTintement[5] {11, 12, 14, 15};
  // (49)-1     maria(samedi et fête de Marie), tintement mi (110) 1'50
  // (50)-2     mémoire, tintement sol,                      (220) 2,05
  // (52)-4     férie, tintement si,                         (430) 2'25
  // (49)-1     2_ordre, tintement mi,                       (310)
  // (49)-1   dimanche, tintement mi,                        (510)
  // (50)-2)   3_ordre, tintement sol                        (620)
  // (55)-4-2-1(7) 1_ordre, tintement si/sol/mi              (770) 2'15
  //cloche unique------------------------------
  int npin;
  int n_pin;
  switch (c) {
    case 49 : npin = 0; break; //mi
    case 50 : npin = 1; break; //sol
    case 52 : npin = 3; break; //si
    case 55 : npin = 3; break; //si
  }
  temps = 5000;
  //3x3coups séparés de 5s avec 10s de pause entre les groupes
  for (int i = 1; i < 10; i++) {
    digitalWrite(tabTintement[npin], HIGH);
    delay(tabImpulsion[npin]);
    digitalWrite(tabTintement[npin], LOW);
    if (i % 3 == 0) {
      if (c == 55) {
        //1er ordre, 3 cloches
        if (npin == 3) {
          npin = npin - 2;
        }
        else {
          npin = npin - 1;
        }
      }
      if ( i == 9) {
        break; //on saute la pause
      }
      //changement de cloche
      delay(temps * 2);
    }
    else {
      delay(temps - tabImpulsion[npin]);
    }
  }
  delay(1000);
  c = 0;
}
//cloche, nombre de coups
void angelus(int v, int t) {
  tintementAngelus(t);
  t = 0;
  delay(5000);
  volee(v);
  v = 0;
}

//-----------------------------------------------------------------------------------
//boucle sur une 1/2seconde, timbre toutes les secondes, cloche toutes les 5 secondes(1mn)
void tintement(byte cl, int cp) {
  //cp est 110 == 1x10, 120 = 1x12, 135 = 3x5(à h-5), 133 = 3x3(angelus)
  //                     49  50  51  52
  //                     mi  sol ti  si
  //int tabTintement[5] {11, 12, 14, 15};
  digitalWrite(enCours, HIGH);
  //tabImpulsion[impuls] donne la valeur pour chaque cloche
  int pin = cl - 49;//récupère le n° du tableau
  //  int tabTintement[5] {11, 12, 14, 15};
  int npin = tabTintement[pin];
  int tabT;//n° tabCoup[] {2000, 2000, 1000, 2000}
  int pause;
  int c_p;
  int diviseur;
  int j = 0;//compteur des divisions
  tabT = cl - 49;
  if (sortie == 0) {
    //    case 101 : c_p = 1; break;(e)
    //    case 105 : c_p = 5; break;(i)
    //    case 110 : c_p = 10; break;(n)
    //    case 112 : c_p = 12; break;(p)
    //    case 109 : c_p = 9; divseur = 3; break;(m) angelus, pause5s entre coup, 10 entre groupes
    //    case 115 : c_p = 15; divseur = 5;break;(s) uniquement timbre, 1cp/s, 5s entre groupes
    c_p = cp - 100;
    if (c_p == 9) { //angelus
      if (npin == 14) { //timbre ne devrait jamais arriver
        temps = 1400;
        pause = 5000;
      }
      else {
        temps = 5000;
        pause = 2000;
      }
    }
    else {
      if (npin == 14) { //timbre
        temps = 1000 - tabImpulsion[2];
        pause = 5000;
      }
      else {
        temps = 1400; //
        pause = 2000;
      }
    }
    //int tabImpulsion[5] {150, 150, 400, 150}; //récupéré avec [t]
    for (byte i = 1; i <= c_p; i++) {
      if (npin == 14) { //timbre, 1coup = 0,39 s
        temps = 940 - tabImpulsion[2];
      } else {
        if ( c_p == 1) {
          temps = 400;
        } else {
          temps = 5000;
        }

      }
      digitalWrite(npin, HIGH) ;//cloche

      delay(tabImpulsion[tabT]);
      digitalWrite(npin, LOW) ;//cloche
      if (( c_p % 3 == 0) && ( c_p != 12)) {
        if ((i % (c_p / 3) == 0) && (i != 1)) {
          if (npin == 14) {
            pause = 3000;
          } else {
            pause = 5000;
          }
          //pause de 5 secondes entre les groupes
          j++;
          if ( j == 3) {
            delay(1000);
          } else {
            delay(pause);
          }
        }
      }
      delay(temps);
      delay(30);
    }
    digitalWrite(enCours, LOW);
  } else {//glas, désactivation de tous les boutons sauf 5 et 6
    if ((poussoir == 5) || (poussoir == 6)) {
      digitalWrite(npin, HIGH) ;//cloche
      delay(tabImpulsion[tabT]);
      digitalWrite(npin, LOW) ;//cloche
      digitalWrite(enCours, LOW);
    }
  }
}

void poussoir_act(byte p, byte c) {
  if (sortie == 1) {//pendant la volée libre, seul deux boutons sont actifs
    switch (p) {
      case 5 : tintement(50, 'e');//sol
        break; //sol
      case 6 : tintement(52, 'e');//si
        break; //si
    }
  } else {
    switch (p) {
      //volées
      case 1 : volee(49);
        break; //mi
      case 2 : volee(50);
        break; //sol
      case 3 : volee(52);
        break; //si
      //tintements
      case 4 : tintement(49, 'e');//mi
        break; //mi
      case 5 : tintement(50, 'e');//sol
        break; //sol
      case 6 : tintement(52, 'e');//si
        break; //si
      case 7 : //lever (t = 116) Kazimieras + sk : (0tu)
        tintement(52, 'e');
        delay(2000);
        tintement(51, 'i');
        break; //ti
      //pour les trois boutons suivants, le voyant doit être géré
      case 8 : digitalWrite(8, HIGH);//angelus férie    //                  49  50  51  52
        angelus(52, 52);//feteII 51, 49                 //       cloche     mi  sol ti  si
        delay(40);                                      //       pin        11  12  14  15
        digitalWrite(8, LOW);
        break;
      case 9 : digitalWrite(9, HIGH); //ti 1x10
        tintement(51, 'n');
        digitalWrite(9, LOW);
        break;
      case 10 : digitalWrite(10, HIGH); //ti 3x5+ 12xcloche +pause +1coup
        tintement(51, 's');
        digitalWrite(enCours, HIGH);
        delay(1000);
        tintement(52, 'p');
        digitalWrite(enCours, HIGH);
        delay(228000);
        tintement(52, 'e');
        digitalWrite(10, LOW);
        break;
      //------------------------clavier virtuel, groupe de programme------
      case 11 : //lever (t = 116) Kazimieras + sk : (0tu)
        tintement(52, 'e');
        digitalWrite(enCours, HIGH);
        delay(2000);
        tintement(51, 'i');
        poussoir = 0;//
        break;
      case 12 : // (u = 117) sk2 + cloche
        tintement(51, 's');//toujours timbre       s = 3x5
        digitalWrite(enCours, HIGH);// 49 mi  (0ut) p = 12 Marija
        delay(2000);//                 50 sol (0uu)        Petras
        tintement(cloche, 'p');//      52 si  (0uw)        Kazimieras
        poussoir = 0;//
        break;
      case 13 : //(v=118) volée mi longue, 2mn
        digitalWrite(enCours, HIGH);
        digitalWrite(16, HIGH);
        delay(120000);
        digitalWrite(16, LOW);
        break;
      case 14 : //(w=119)volée mi libre, contrôlé par lignes 512 ss
        digitalWrite(enCours, HIGH);
        digitalWrite(16, HIGH);
        sortie = 1;
        break;
      case 15 : {//(x=120)annonce de décès
          long int tabpose[4] {15000, 20000, 25000};
          int tabcloche[3] {52, 50, 49};
          digitalWrite(enCours, HIGH);
          for (int i = 1; i < 13; i++) {//boucle des 12 cycles
            for (int j = 0; j < 3; j++) { //boucle des cloches si, sol, mi
              tintement(tabcloche[j], 'e');
              Serial.print(i); Serial.print(" "); Serial.print(tabcloche[j]); Serial.print(" ");Serial.println(tabpose[j]);
              digitalWrite(enCours, HIGH);
              delay(tabpose[j]);
              if (j == 3) {
                j = 0;
              }
            }
          }
          digitalWrite(enCours, LOW);
          poussoir = 14;
        }
        break;
      default : poussoir = 0;
        break;
    }
  }
}

//-----------------------------------------------------------------------------
void loop() {
//  Serial.println(sensorBase);delay(200);
  if (Serial.available() > 0) { // si des données entrantes sont présentes
    commandeRecue = Serial.read();
    delay(30);
    //on récupère seulement les valeurs supérieur à 47 : 0=48 et < 126
    if (( commandeRecue > 47) && ( commandeRecue < 126)) {
      tabRC[k] = commandeRecue;
      k++;
    }
    //Groupe de sonnerie, poussoirs virtuels
    // t = 116, 11  /---/1  105 = j
    // u = 117, 12  /---/2  106 = k
    // v = 118, 13  /---/3  107 = l
    // w = 119, 14  /---/4  108 = m
    // x = 120, 15  /---/5  109 = n
    // y = 121, 16  /---/6  110 = o
    // z = 122, 17  /---/7  112 = p
    // { = 123, 18  /---/8  113 = q
    // | = 124, 19  /---/9  114 = r
    // } = 125, 20  /---/10 115 = s
    if ( k == 4) {
      //on a récupéré toutes les données, on vide ce qui peut rester
      Serial.flush();
      //      digitalWrite(enCours, HIGH);//début led programme en cours
      if (tabRC[2] >= 104) {
        cloche = tabRC[3] - 67;
        poussoir = tabRC[2] - 105;
      } else {
        if ((tabRC[1] > 48) && (tabRC[2] > 48)) {
          //angelus, ligne 220
          angelus(tabRC[1], tabRC[2]);
          digitalWrite(enCours, LOW);
        }
        else {
          if (tabRC[1] > 48) {
            //volees, ligne 89
            volee(tabRC[1]);
            delay(30);
            digitalWrite(enCours, LOW);
          }
          else if (tabRC[2] > 48) {
            //tintements, ligne 229
            tintement(tabRC[2], tabRC[3]);
            delay(30);
            digitalWrite(enCours, LOW);
          }
        }
      }
      tabRC[1] = 0;
      tabRC[2] = 0;
      tabRC[3] = 0;
      k = 1;
    }
    digitalWrite(enCours, LOW);
  }
  if ((k > 1) && (k < 4)) {
    // k = 1 ou incrémenté jusqu'à 4
    // s'il reste à une valeur intermédiaire, il a manqué un caractère, on remet k =1
    l++;
    if (l == 10) {
      l = 1;
      k = 1;
    }
  }

  //------------------boutons poussoirs
  sensorValue = analogRead(sensorPin);

  if (sensorValue < (rc - 5)) {
    //relecture pour passer le rebond du bouton après pause
    delay(100);
    sensorValue = analogRead(sensorPin);
    delay(100);
    //            pour une valeur reçue de//     256
    if (sensorValue < 53) {               //      53
      poussoir = 1;
    } else if (sensorValue < (rc / 3.2)) {//      80
      poussoir = 2;
    } else if (sensorValue < (rc / 2.415)) {//   106
      poussoir = 3;
    } else if (sensorValue < (rc / 1.984)) {//   129
      poussoir = 4;
    } else if (sensorValue < (rc / 1.684)) {//   152
      poussoir = 5;
    } else if (sensorValue < (rc / 1.471)) {//   174
      poussoir = 6;
    } else if (sensorValue < (rc / 1.319)) {//   194
      poussoir = 7;
    } else if (sensorValue < (rc / 1.201)) {//   213
      poussoir = 8;
    } else if (sensorValue < (rc / 1.103)) {//   232
      poussoir = 9;
    } else if (sensorValue < (rc / 1.028)) {//   249
      poussoir = 10;
    }
  }
  if (poussoir > 0) {
    digitalWrite(enCours, HIGH);//début programme
    poussoir_act(poussoir, cloche);
    poussoir = 0;
    digitalWrite(enCours, LOW);
  }
  if (sortie == 1) {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
      digitalWrite(16, LOW);//arrêt de la volée
    } else if (currentMillis - previousMillis >= (interval * 2)) {
      sortie = 0;//désactivation de la protection de tintements
    }
  }
}
