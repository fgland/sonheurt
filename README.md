Gestion automatisé des heures monastiques du monastère de Palendriai en Lituanie.

La gestion du calendrier liturgique est géré en php, actuellement non finalisée.

Les commandes automatiques sont gérées en javascript/jquery.

Le dépot contient un dossier pour les fichiers arduino et un pour les fichiers du serveur http du raspberry.

L'Arduino est contrôlé par l'interface tactile du raspberry via php_serial ou par le clavier branché directement sur une entrée.

Voir le wiki en cours de rédaction, pour le détail.

On peut tester l'interface partiellement traduite à l'adresse : http://sonheurt.landroware.ovh/index.php
